---
title: 'BBB Sprat'
fontsfiles:
    -
        style: 'Regular Regular'
        font:
            'theme://css/fonts/BBBSprat-Regular.woff':
                name: BBBSprat-Regular.woff
                type: font/woff
                size: 29248
                path: 'theme://css/fonts/BBBSprat-Regular.woff'
    -
        style: 'Regular Light'
        font:
            'theme://css/fonts/BBBSprat-Regularlight.woff':
                name: BBBSprat-Regularlight.woff
                type: font/woff
                size: 30440
                path: 'theme://css/fonts/BBBSprat-Regularlight.woff'
    -
        style: 'Regular Thin'
        font:
            'theme://css/fonts/BBBSprat-RegularThin.woff':
                name: BBBSprat-RegularThin.woff
                type: font/woff
                size: 29904
                path: 'theme://css/fonts/BBBSprat-RegularThin.woff'
    -
        style: 'Condensed Regular'
        font:
            'theme://css/fonts/BBBSprat-CondensedRegular.woff':
                name: BBBSprat-CondensedRegular.woff
                type: font/woff
                size: 27200
                path: 'theme://css/fonts/BBBSprat-CondensedRegular.woff'
    -
        style: 'Condensed Light'
        font:
            'theme://css/fonts/BBBSprat-CondensedLight.woff':
                name: BBBSprat-CondensedLight.woff
                type: font/woff
                size: 29260
                path: 'theme://css/fonts/BBBSprat-CondensedLight.woff'
    -
        style: 'Condensed Thin'
        font:
            'theme://css/fonts/BBBSprat-CondensedThin.woff':
                name: BBBSprat-CondensedThin.woff
                type: font/woff
                size: 27268
                path: 'theme://css/fonts/BBBSprat-CondensedThin.woff'
    -
        style: 'Extended Regular'
        font:
            'theme://css/fonts/BBBSprat-ExtendedRegular.woff':
                name: BBBSprat-ExtendedRegular.woff
                type: font/woff
                size: 28508
                path: 'theme://css/fonts/BBBSprat-ExtendedRegular.woff'
    -
        style: 'Extended Light'
        font:
            'theme://css/fonts/BBBSprat-ExtendedLight.woff':
                name: BBBSprat-ExtendedLight.woff
                type: font/woff
                size: 30496
                path: 'theme://css/fonts/BBBSprat-ExtendedLight.woff'
    -
        style: 'Extended Thin'
        font:
            'theme://css/fonts/BBBSprat-ExtendedThin.woff':
                name: BBBSprat-ExtendedThin.woff
                type: font/woff
                size: 28136
                path: 'theme://css/fonts/BBBSprat-ExtendedThin.woff'
sentence: 'Mes ami·es sont ému·es.'
font:
    author: 'Eugénie Bidaut, Julie Patard et al.'
    website: 'https://xn--eugniebidaut-deb.eu/'
    date: '2024'
    license: OFL
    download: 'https://gitlab.com/bye-bye-binary/bbb-sprat/-/archive/main/bbb-sprat-main.zip'
    link: 'https://gitlab.com/bye-bye-binary/bbb-sprat#'
    fontspecimen: 'https://gitlab.com/bye-bye-binary/bbb-sprat/-/raw/main/20220620-BBBSprat_specimen-glyphes-inclusifs.pdf?ref_type=heads&inline=false'
publish_date: '11-09-2024 17:22'
---

La Sprat a été dessinée par Ethan Nakache et distribuée par la fonderie Collletttivo. En 2022, Eugénie Bidaut et Julie Patard lui ajoutent des caractères post-bianires pour le festival bruxellois FAME.