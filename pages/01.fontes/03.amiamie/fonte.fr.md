---
title: Amiamie
font:
    author: 'Mirat Masson et al.'
    license: CUTE
    download: 'https://gitlab.com/Mirat-Masson1/Amiamie-Typeface/-/archive/main/Amiamie-Typeface-main.zip'
    link: 'https://gitlab.com/Mirat-Masson1/Amiamie-Typeface'
    website: www.miratmasson.com
    date: '2022'
    fontspecimen: 'https://gitlab.com/Mirat-Masson1/Amiamie-Typeface/-/blob/main/Specimen.pdf?ref_type=heads'
published: true
sentence: 'Je veux un·e président·e qui vit sans clim,'
fontsfiles:
    -
        style: Light
        font:
            'theme://css/fonts/Amiamie-Light.woff':
                name: Amiamie-Light.woff
                type: font/woff
                size: 28924
                path: 'theme://css/fonts/Amiamie-Light.woff'
    -
        style: 'Light Italic'
        font:
            'theme://css/fonts/Amiamie-LightItalic.woff':
                name: Amiamie-LightItalic.woff
                type: font/woff
                size: 28872
                path: 'theme://css/fonts/Amiamie-LightItalic.woff'
    -
        style: Regular
        font:
            'theme://css/fonts/Amiamie-Regular.woff':
                name: Amiamie-Regular.woff
                type: font/woff
                size: 29460
                path: 'theme://css/fonts/Amiamie-Regular.woff'
    -
        style: Italic
        font:
            'theme://css/fonts/Amiamie-Italic.woff':
                name: Amiamie-Italic.woff
                type: font/woff
                size: 28812
                path: 'theme://css/fonts/Amiamie-Italic.woff'
    -
        style: Black
        font:
            'theme://css/fonts/Amiamie-Black.woff':
                name: Amiamie-Black.woff
                type: font/woff
                size: 29916
                path: 'theme://css/fonts/Amiamie-Black.woff'
    -
        style: 'Black Italic'
        font:
            'theme://css/fonts/Amiamie-BlackItalic.woff':
                name: Amiamie-BlackItalic.woff
                type: font/woff
                size: 29284
                path: 'theme://css/fonts/Amiamie-BlackItalic.woff'
    -
        style: 'Regular Round'
        font:
            'theme://css/fonts/Amiamie-RegularRound.woff':
                name: Amiamie-RegularRound.woff
                type: font/woff
                size: 34548
                path: 'theme://css/fonts/Amiamie-RegularRound.woff'
    -
        style: 'Italic Round'
        font:
            'theme://css/fonts/Amiamie-ItalicRound.woff':
                name: Amiamie-ItalicRound.woff
                type: font/woff
                size: 35000
                path: 'theme://css/fonts/Amiamie-ItalicRound.woff'
    -
        style: 'Black Round'
        font:
            'theme://css/fonts/Amiamie-BlackRound.woff':
                name: Amiamie-BlackRound.woff
                type: font/woff
                size: 34648
                path: 'theme://css/fonts/Amiamie-BlackRound.woff'
    -
        style: 'Black Italic Round'
        font:
            'theme://css/fonts/Amiamie-BlackItalicRound.woff':
                name: Amiamie-BlackItalicRound.woff
                type: font/woff
                size: 35288
                path: 'theme://css/fonts/Amiamie-BlackItalicRound.woff'
publish_date: '10-06-2024 15:44'
---

Amiamie est une police de caractères imaginée entre ami·es et inspirée de la classique Helvetica. Amiamie a été conçu..e pour que l’on puisse s’écrire des mots doux dans une multitude de graisses.

Amiamie est une fonte pour tous·tes les auteur·ices et poètes·ses qui souhaitent être les nouvelle..aux héro·ïnes inclusif·ves d’une écriture libre. Amiamie est aussi pour celui·elle qui est heureux·se et fier·e de se représenter dans une linéale non-binaire.

Amiamie prolonge l’histoire de l’Helvetica. D'abord nommée Neue Haas Grotesk, elle prend son nom définitif en 1960. Cinquante ans plus tard, Christian Schwartz numérise les fontes originales de Max Miedinger et les rebaptise de leur nom originel.

En 2014, Sora Sagano s’inspire de toute cette ligne pour créer l’Aileron et la diffuse en open source, libre de droits. Mirat-Masson se la réapproprie ensuite pour l’augmenter de glyphes non-binaires.