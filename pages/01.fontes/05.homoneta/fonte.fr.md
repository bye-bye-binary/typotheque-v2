---
title: Homoneta
sentence: 'I·Els disent qu’i·els partent de zéro.'
font:
    author: 'Quentin Lamouroux'
    website: 'https://quentinlamouroux.com/'
    date: '2022'
    license: CUTE
    download: 'https://gitlab.com/lamourouxqu/homoneta/-/blob/main/homoneta.zip?ref_type=heads'
    link: 'https://gitlab.com/lamourouxqu/homoneta'
    fontspecimen: 'https://quentinlamouroux.com/#homo'
fontsfiles:
    -
        style: Italic
        font:
            'theme://css/fonts/Homoneta-Italic.woff':
                name: Homoneta-Italic.woff
                type: font/woff
                size: 32848
                path: 'theme://css/fonts/Homoneta-Italic.woff'
    -
        style: Regular
        font:
            'theme://css/fonts/Homoneta-Regular.woff':
                name: Homoneta-Regular.woff
                type: font/woff
                size: 35488
                path: 'theme://css/fonts/Homoneta-Regular.woff'
publish_date: '10-06-2024 15:45'
---

Le projet typographique Homoneta s’active dans une fiction – Et Homo Créa la Matière – à la manière d’un mythe dans lequel on suit l’évolution d’Homo, un personnage mystique & emblématique symbolisant la condition humaine&#8239;: l’aptitude à créer, former des aspirations et d’y croire. À i·el seul·e, Homo incarne l’ensemble de l’humanité & la création de la monnaie&#8239;: une construction imaginaire & un système universel dont le fonctionnement opère uniquement par croyance & confiance collective.

Plusieurs notions ont guidé les recherches typographiques d’Homoneta, à commencer par la notion d’atavisme. En biologie évolutive, l’atavisme est la réapparition d’un caractère ancestral chez un·e individu·e qui ne devrait pas le posséder&#8239;; un trait perdu ou transformé au cours de l’évolution. L’atavisme – ici rattaché à la typographie – peut s’apparenter à l’hybridation typographique, ce qui ouvre naturellement mes recherches vers les classifications typographiques et «&#8239;les fontes dites “hybrides” à notre époque où les formes typographiques sont empruntées, modifiées et/ou renouvelées à l’infini, et tentent de mettre à mal toute catégorie&#8239;» Laurent Müller, *Essai d’une refonte des classifications, essai d’une reclassification de fontes*, 2018.

Homoneta est composée de caractères latins aux héritages typographiques multiples et brouille les pistes relatives à l’époque dans laquelle la·e spectateur·ice se trouve – l’incitant à se poser des questions presque existentielles telles que «&#8239;qu’est-ce que je regarde&#8239;?&#8239;» & «&#8239;d’où je regarde&#8239;?&#8239;». Homoneta tente de voyager à travers les âges et de croiser les époques en empruntant des caractéristiques typographiques spécifiques aux typographies avec empattements & linéales et aux écritures anciennes & manuscrites. Alors que ses empattements sont formés par effet d’optique par une transition tranchée net sur le fût qui donne un effet optique arrondi, son axe de construction est quant à lui droit – à la manière d'une linéale. L’une des autres caractéristiques réside dans lutilisation marquées des ligatures en référence à l’écriture et ouvrages manuscrites. Elles vont des classiques jusqu’aux plus ornementales et expérimentales, en passant par les inclusives. Homoneta tente d’une part de trouver son caractère mystique dans des ligatures qui paraissent comme de nouveaux graphèmes, d’autre part elle s’accompagne d’une réelle envie d’appuyer la non-binarité du personnage Homo. Certains élements sont directement liés à l’outil plume manuscrite comme les terminaisons en goutte & la languette du “e” – un emprunt typographique de l’époque moyenâgeuse détourné comme une partie atrophiée qui sert de base à la constuction des ligatures. D’autres lettres sont nettement tranchées aux attaques ce qui semble être une partie typographique qu’elles auraient perdu au fil de l’évolution. Quant aux capitales, elles sont dessinées à la manière des lettres Romaines aux terminaisons et empattements pointu·es. Ici les ligatures sont dessinées à la manière de monogrammes faisant réapparaître l’usage de l’ornement et, comme ceux frappés sur la monnaie, évoquent une autorité.