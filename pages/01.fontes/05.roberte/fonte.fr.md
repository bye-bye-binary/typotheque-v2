---
title: Roberte
sentence: 'Robert·e est belle·au, aventureux·se et fier·e.'
font:
    author: 'Eugénie Bidaut'
    website: 'https://xn--eugniebidaut-deb.eu/'
    date: '2023'
    license: CUTE
    download: 'https://gitlab.com/Eugenie-B/roberte/-/archive/main/roberte-main.zip'
    link: 'https://gitlab.com/Eugenie-B/roberte'
    fontspecimen: 'https://gitlab.com/Eugenie-B/roberte/-/raw/main/specimen-roberte.pdf?ref_type=heads&inline=false'
fontsfiles:
    -
        style: Regular
        font:
            'theme://css/fonts/Roberte-Regular.woff':
                name: Roberte-Regular.woff
                type: font/woff
                size: 28044
                path: 'theme://css/fonts/Roberte-Regular.woff'
publish_date: '16-06-2024 15:26'
---

Robert·e est une stroke fonte sans courbes dessinée librement à partir des italiques de Robert Granjon. Robert·e est une italique sans romain car Robert·e n’en a pas besoin. Robert·e est libre, Roberte est indépendant·e. Roberte est open source. Robert·e a des caractères post-binaires. Robert·e a un tracé rapide parce que Robert·e n’a pas le temps de niaiser.