---
title: Ductus
fontsfiles:
    -
        style: 'Regular Regular'
        font:
            'theme://css/fonts/DuctusRegular.woff':
                name: DuctusRegular.woff
                type: font/woff
                size: 38668
                path: 'theme://css/fonts/DuctusRegular.woff'
    -
        style: 'Regular Geometric'
        font:
            'theme://css/fonts/DuctusGeometric.woff':
                name: DuctusGeometric.woff
                type: font/woff
                size: 43260
                path: 'theme://css/fonts/DuctusGeometric.woff'
    -
        style: 'Regular Calligraphic'
        font:
            'theme://css/fonts/DuctusCalligraphic.woff':
                name: DuctusCalligraphic.woff
                type: font/woff
                size: 43420
                path: 'theme://css/fonts/DuctusCalligraphic.woff'
    -
        style: 'Mono Regular'
        font:
            'theme://css/fonts/DuctusMonoRegular.woff':
                name: DuctusMonoRegular.woff
                type: font/woff
                size: 26940
                path: 'theme://css/fonts/DuctusMonoRegular.woff'
    -
        style: 'Mono Geometric'
        font:
            'theme://css/fonts/DuctusMonoGeometric.woff':
                name: DuctusMonoGeometric.woff
                type: font/woff
                size: 28540
                path: 'theme://css/fonts/DuctusMonoGeometric.woff'
    -
        style: 'Mono Calligraphic'
        font:
            'theme://css/fonts/DuctusMonoCalligraphic.woff':
                name: DuctusMonoCalligraphic.woff
                type: font/woff
                size: 26820
                path: 'theme://css/fonts/DuctusMonoCalligraphic.woff'
sentence: 'Un·e auteur·ice programmateur·ice.'
font:
    author: 'Amélie Dumont'
    website: 'https://www.amelie.tools/'
    date: '2022'
    license: CUTE
    download: 'https://gitlab.com/ameliedumont/fonts/-/archive/master/fonts-master.zip'
    link: 'https://gitlab.com/ameliedumont/fonts/-/tree/master/Ductus'
    fontspecimen: 'https://gitlab.com/ameliedumont/fonts/-/raw/master/Ductus/monospace/specimen_mono_calligraphic.pdf?inline=false'
publish_date: '10-06-2024 16:15'
---

Ductus est une famille typographique inspirée de la calligraphie romaine. Les glyphes sont composés de plusieurs tracés reprenant les ductus traditionnels suivant lesquels les lettres étaient dessinées. Ces typographies ont été dessinées à l'aide de l'outil Metapost, qui permet de définir la position de points et de les relier avec différentes fonctions. On y dessine ses glyphes par un squelette central, un « stroke », sur lequel on vient ensuite appliquer un outil dont la forme peut varier (arrondi, biseauté, rectangulaire). Cet outil propose un parallèle intéressant avec la pratique calligraphique qui consiste finalement aussi à dessiner des lettres d'après un tracé central et à laisser la forme et la pente de l'outil déterminer les contours de ce tracé. Les trois graisses dans lesquelles se décline la famille Ductus font appel aux trois formes de stylo principales de Metapost : Pencircle, Penrazor et Pensquare.