---
title: 'BBB Baskervvol'
sentence: 'Je veux un·e gouin·e pour président·e.'
font:
    author: 'Bye Bye Binary et al.'
    license: CUTE
    download: 'https://gitlab.com/bye-bye-binary/baskervvol/-/archive/master/baskervvol-master.zip'
    link: 'https://gitlab.com/bye-bye-binary/baskervvol'
    date: '2018'
fontsfiles:
    -
        style: Base
        font:
            'theme://css/fonts/BBBBaskervvol-Base.woff':
                name: BBBBaskervvol-Base.woff
                type: font/woff
                size: 51776
                path: 'theme://css/fonts/BBBBaskervvol-Base.woff'
    -
        style: Fondue
        font:
            'theme://css/fonts/BBBBaskervvol-Fondue.woff':
                name: BBBBaskervvol-Fondue.woff
                type: font/woff
                size: 52008
                path: 'theme://css/fonts/BBBBaskervvol-Fondue.woff'
publish_date: '10-06-2024 15:44'
---

Le Baskervvol est une reprise par BBB du Baskervville de l’Atelier national de Recherche typographique (ANRT), lui-même repris du Baskerville de Claude Jacob de 1784, dessiné par John Baskerville en 1750. John Baskerville est un cas exemplaire de l’invisibilisation des femmes dans l’histoire de la typographie. Sarah Eaves, sa compagne et associée, qui reprit l’imprimerie à la mort de Baskerville, n’a jamais été créditée pour son travail bien qu’elle ait largement participé à l’élaboration de caractères et d’imprimés commercialisés par son mari [^1]. En 1996, la typographe Zuzana Licko dessinera un caractère en son honneur. Depuis 2018, le Baskervvol est augmenté collectivement de glyphes inclusifs.

Là où les universités exigent pour l’écriture d’articles scientifiques l’utilisation du Times New Roman, sous licence privative et aux droits réservés, l’utilisation du Baskervvol, une police de caractères présentant une autorité stylistique et historique similaire, mais libérée par sa licence, permet l’introduction de glyphes non binaires dans les lieux normatifs de diffusion des savoirs.

Dessinateur·ices des glyphes post-binaires : Eugénie Bidaut, Julie Colas, Camille Circlude, Louis Garrido, Enz@ Le Garrec, Ludi Loiseau, Édouard Nazé, Julie Patard, Marouchka Payen, Mathilde Quentin) 
Dessinateur·ices de la Baskervville ANRT : Alexis Faudot, Rémi Forte, Morgane Pierson, Rafael Ribas, Tanguy Vanlaeys, Rosalie Wagner.

[^1]: Loraine FURTER. [Crystal Clear, 2020](https://depatriarchisedesign.com/2020/02/02/crystal-clear-by-loraine-furter/) 