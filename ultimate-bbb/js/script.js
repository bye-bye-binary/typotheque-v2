document.addEventListener('DOMContentLoaded', () => {

    const homeScreen = document.getElementById("home-screen");

    let inactivityTime = 25000;
    let inactivityTimer;

    function showHomeScreen() {
        homeScreen.style.display = "block";
    }

    function hideHomeScreen() {
        homeScreen.style.display = "none";
    }

    function resetInactivityTimer() {
        hideHomeScreen();
        clearTimeout(inactivityTimer);
        inactivityTimer = setTimeout(showHomeScreen, inactivityTime);
    }

    document.addEventListener("mousemove", resetInactivityTimer);
    document.addEventListener("keydown", resetInactivityTimer);
    document.addEventListener("scroll", resetInactivityTimer);
    document.addEventListener("click", resetInactivityTimer);

    resetInactivityTimer();


    setTimeout(() => {
        document.querySelectorAll('.sentence').forEach(el => {

            const elementWidth = el.offsetWidth;
            const parentWidth = el.parentElement.scrollWidth;
            const totalWidth = elementWidth + parentWidth;
            const speed = 700;

            el.style.animationDuration = Math.round((totalWidth / speed)) + 's';
            document.documentElement.style.setProperty('--animation-duration', el.style.animationDuration);
        });
    }, 0);


    /////// TOGGLING AND STORING DARK-MODE ///////

    const toggleDarkBtn = document.getElementById('toggle-dark');

    if (localStorage.getItem('darkModeEnabled') === 'true') {
        document.body.classList.add('dark-mode');
    } else {
        document.body.classList.remove('dark-mode');
    }

    toggleDarkBtn.addEventListener('click', function () {
        const isDarkModeEnabled = document.body.classList.toggle('dark-mode');
        localStorage.setItem('darkModeEnabled', isDarkModeEnabled);
    });

    /////// MOVING GRADIENT ON MOUSE POS ///////

    const elements = document.querySelectorAll(".fonte-info");

    elements.forEach(el => {
        el.addEventListener("mousemove", (e) => {
            windowWidth = window.innerWidth;
            mouseXpercentage = Math.round(e.pageX / windowWidth * 100);
            el.style.background = 'radial-gradient(circle at ' + mouseXpercentage + '%, rgba(0,255,59,1) 0%, rgba(255,255,255,0) 53%)';
        });
    });

});

/////// FETCHING DARK-MODE STATE ///////

function setTheme() {
    const darkModeEnabled = localStorage.getItem('darkModeEnabled');
    if (darkModeEnabled === 'true') {
        document.body.classList.add('dark-mode');
    } else {
        document.body.classList.remove('dark-mode');
    }
};

/////// FETCHING CLOUD STATE ///////

const cloudDisplay = sessionStorage.getItem('cloudDisplay');

if (cloudDisplay === null) {
    sessionStorage.setItem('cloudDisplay', true);
}

function setCloudDisplay() {
    const cloudDisplay = sessionStorage.getItem('cloudDisplay');

    const arrow = document.querySelectorAll(".arrow");
    const sidebar = document.getElementById("hello");

    if (typeof (sidebar) != 'undefined' && sidebar != null) {
        if (cloudDisplay === 'false') {
            sidebar.classList.add("translate");
            arrow[0].classList.add("translate");
        } else {
            sidebar.classList.remove("translate");
            arrow[0].classList.remove("translate");
        }
    }
}

function change_style() {
    selectElement = document.querySelector('#font_weights');
    if (typeof (selectElement) != 'undefined' && selectElement != null) {
        font_style = selectElement.options[selectElement.selectedIndex].value
        document.getElementById("font-body").style.fontFamily = font_style;
    } else {
    }
}

document.addEventListener("DOMContentLoaded", () => {
    setTheme();
    setCloudDisplay();

    change_style();
});

// footer

const entriesFooter = document.querySelectorAll(".footer-link");
const details = document.querySelectorAll("details");

function getAnchors() {

    details.forEach((detail) => {
        const anchorArray = detail.querySelectorAll(".anchor");
        var dataArray = [];

        anchorArray.forEach((anchor) => {
            dataArray.push(anchor.id);
        })

        const attributeValue = dataArray[0];
        detail.setAttribute('data-chapitre', attributeValue);
    });

    setTimeout(function () {
        document.querySelector('#font-sentence p').style.visibility = 'visible'
    }, 50)
};

getAnchors();