---
title: PicNic
sentence: 'Toustes les ami·es explorateur·ices sauvages, poétiques et bucoliques.'
font:
    author: 'Mariel Nils'
    website: 'https://www.instagram.com/mariel.nils/'
    date: '2022'
    license: CUTE
    download: 'https://gitlab.com/bye-bye-binary/picnic/-/archive/main/picnic-main.zip'
    link: 'https://gitlab.com/bye-bye-binary/picnic'
    fontspecimen: 'https://gitlab.com/mariellenils/PicNic/-/raw/main/documentation/tableclothes-hfgk/tableclothes-hfgk-01.jpg'
fontsfiles:
    -
        style: Regular
        font:
            'theme://css/fonts/PicNic.woff':
                name: PicNic.woff
                type: font/woff
                size: 98380
                path: 'theme://css/fonts/PicNic.woff'
publish_date: '10-06-2024 16:26'
---

PicNic est un caractère organique abritant une nuée de ligatures contextuelles et de ligatures inclusives. Il a été diffusé sur Velvetyne et NoFoundry. Ce caractère conviendra à toustes les ami·es explorateur·ices sauvages, poétiques et bucoliques pour leurs excursions les plus aventureuses. Les formes sont inspirées de la trajectoire d'une goutte d'eau sur une toile cirée, les ombres des feuilles des arbres sur le tissu étendu dans l'herbe, de ce chemin précis que la fourmi emprunte jusqu'au melon.