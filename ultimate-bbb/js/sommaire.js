document.addEventListener('DOMContentLoaded', () => {


     /////// TOGGLING AND STORING DARK-MODE ///////

     const toggleDarkBtn = document.getElementById('toggle-dark');

     if (localStorage.getItem('darkModeEnabled') === 'true') {
         document.body.classList.add('dark-mode');
     } else {
         document.body.classList.remove('dark-mode');
     }
 
     toggleDarkBtn.addEventListener('click', function () {
         const isDarkModeEnabled = document.body.classList.toggle('dark-mode');
         localStorage.setItem('darkModeEnabled', isDarkModeEnabled);
     });
     
    function setTheme() {
        const darkModeEnabled = localStorage.getItem('darkModeEnabled');
        if (darkModeEnabled === 'true') {
            document.body.classList.add('dark-mode');
        } else {
            document.body.classList.remove('dark-mode');
        }
    };

    setTheme();

    const entriesLinks = document.querySelectorAll("#sommaire-content > ul:not(ul ul)");
    const details = document.querySelectorAll("details");

    function getAnchors() {

        details.forEach((detail) => {
            const anchorArray = detail.querySelectorAll(".anchor");
            var dataArray = [];

            anchorArray.forEach((anchor) => {
                dataArray.push(anchor.id);
            })

            const attributeValue = dataArray[0];
            detail.setAttribute('data-chapitre', attributeValue);
        });
    };

    getAnchors();

    entriesLinks.forEach((link) => {
        link.addEventListener('click', (el) => {

            const linkId = el.target.getAttribute("href");

            if (!linkId.startsWith('#')) {
                return;
            }

            const trimmedLinkId = linkId.substring(1);
            const targetAnchor = document.getElementById(trimmedLinkId);

            console.log(trimmedLinkId);

            if (targetAnchor) {
                const parentDetails = targetAnchor.closest("details");
                if (parentDetails && !parentDetails.open) {
                    parentDetails.open = true;

                    setTimeout(() => {
                        targetAnchor.scrollIntoView({ behavior: "smooth" });
                    }, 100);
                } else {
                    targetAnchor.scrollIntoView({ behavior: "smooth" });
                }
            }

            details.forEach((detail) => {
                const attribute = detail.getAttribute('data-chapitre');
                if (attribute === trimmedLinkId) {
                    detail.open = true;
                }
            });
        });
    });


    const sections = document.querySelectorAll(".anchor");
    const entries = document.querySelectorAll("#sommaire-content li");

    function isElementInTop100(element) {
        const rect = element.getBoundingClientRect();
        return rect.top <= 100;
    };

    function updateURL() {
        let currentSection = '';

        sections.forEach((section, index) => {
            if (isElementInTop100(section)) {
                currentSection = section.getAttribute('id');
                const currentIndex = index;
                console.log(currentSection);

                entries.forEach((entry, index) => {
                    if (index === currentIndex) {
                        entry.classList.add('progress');
                    } else {
                        entry.classList.remove('progress');
                    }
                });
            }
        });

        if (currentSection) {
            if (window.location.hash !== `#${currentSection}`) {
                history.replaceState(null, null, `#${currentSection}`);
            }
        } else {
            if (window.location.hash) {
                history.replaceState(null, null, ' ');
            }
        }
    }

    window.addEventListener('scroll', updateURL);
    window.addEventListener('load', updateURL);
});
