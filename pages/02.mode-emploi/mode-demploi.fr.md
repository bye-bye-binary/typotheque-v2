---
menu: 'Mode d''emploi'
slug: mode-d-emploi
---

<div id="sommaire-nav" markdown="1">

<div id="sommaire-content" markdown="1">

   * [Installation d’une fonte](#installation)
     * [Sur Windows](#windows)
     * [Sur Mac](#mac)
     * [Sur Linux (Ubuntu)](#linux)
   * [Mode d’emploi des fontes](#mode-demploi)
   * [Contenu et syntaxe](#contenu)
     * [Cas particuliers](#casparticuliers)
   * [Logiciels de traitement de texte et de mise en page](#logiciels)
     * [Word et LibreOffice](#word)
     * [Scribus](#scribus)
     * [Indesign](#indesign)
   * [Si vous êtes dessinateur·ice](#dessinateurice)
   * [Quelques ressources et alliances](#ressources)

</div>

</div>


<div class="content" markdown="1">

# Mode d’emploi

<details markdown="1">

<summary markdown="1">

## Installation d’une fonte <a class="anchor" id="installation"></a>

</summary>

Si vous ne savez pas comment installer une fonte sur votre ordinateur, voici un ensemble de vidéos-tutoriels pour les principaux systèmes d’exploitation.

### Sur Windows <a class="anchor" id="windows"></a>
<iframe src="https://www.youtube.com/embed/P9sveSQy5Ys" title="Comment installer une police de caractères sur Windows 11 ?" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

### Sur Mac <a class="anchor" id="mac"></a>
![](mac.mp4)

### Sur Linux (Ubuntu) <a class="anchor" id="linux"></a>
À venir

</details>

##

<details markdown="1">

<summary markdown="1">

## Mode d’emploi des fontes <a class="anchor" id="mode-demploi"></a>

</summary>

Les fontes de cette typothèque sont encodées de façon à permettre des remplacements automatiques des formes genrées du français (ex: ami·e) en caractères inclusifs et post-binaires.

![Exemple: ami·e devient ami·e](amie-intro.webp)

 L’encodage QUNI assure une compatibilité des fontes post-binaires entre elles. Toutes les fontes peuvent être utilisées avec la même saisie de départ ce qui rend souple le passage de l’une à l’autre. 

![image arbre avec texte saisie mono et 4 variantes en résultat](amie-intro2.webp)

#### Un exemple des remplacements automatiques pendant la saisie&#8239;:
![saisie au clavier avec la BBB Poppins](video-saisie.gif)

</details>

##

<details markdown="1">

<summary markdown="1">

## Contenu et syntaxe <a class="anchor" id="contenu"></a>

</summary>

Pour utiliser ces fontes dans vos textes, il vous suffit de suivre une syntaxe d’édition des formes genrées, et d’activer quelques paramètres dans les logiciels de traitement de texte et de mise en page.

Pour une liste détaillée de la rédaction de toutes les formes genrées, référez-vous à la quatrième colonne «&#8239;syntaxe&#8239;» du [tableau de læ QUNI](/quni/tableau/?target="_blank"?target="_blank").

Deux types de syntaxes fonctionnent. Vous pouvez rédiger vos formes genrées en utilisant un point médian entre la forme masculine et la forme féminine.

![Exemple: ami·e devient ami·e.](amie.webp)

Ou, si le point médian ne vous est pas accessible au clavier, vous pouvez utiliser deux points bas consécutifs à la place.

![Exemple: ami..e devient ami·e devient ami·e.](amie2.webp)

Dans des formes au pluriel, ne répétez pas de point médian entre la forme féminine et le «s».

![Exemple: ami..es ou ami·es devient ami·es](amies3.webp)

### Cas particuliers <a class="anchor" id="casparticuliers"></a>

Pour les terminaisons en «&#8239;-eur/-rice&#8239;» ne doublez pas le «&#8239;r&#8239;».

![Exemple: auteur..ices ou auteur·ices devient auteur·ices](auteurices.webp)

Pour les terminaisons en «&#8239;-eur/-euse&#8239;», ne pas doubler le «&#8239;eu&#8239;»&#8239;:

![Exemple: chercheur·se et pas chercheur·euse](chercheureuse.webp)
![Exemple: chercheur·se devient chercheur·se](chercheureuse2.webp)

Pour les terminaisons en «&#8239;-er/-ère&#8239;», écrivez «&#8239;er·e&#8239;» sans accent ni répétition.

![Exemple: boulanger·e devient boulanger·e](boulangere.webp)

Voici une liste d’exemples de caractères post-binaires et comment les appeler&#8239;:

![Exemples: ce·tte, actif·ve, tous·tes, gros·se, amoureux·se, belle·aux, cell·eux ou celle·ux, mou·lle, ma·on, i·el,, grec·que, du·de la, prince·sse, long·ue, municipau·les, chef·fe, m·paternité ou p·maternité, ho·femme, héro·ïne, rigolo·te, tiers·ce](liste.webp)

Si un cas de figure manque, n’oubliez pas que ce projet est un travail en cours, n’hésitez pas à mettre la main à la pâte collective en nous contactant&#8239;! 
<!-- en ajoutant votre proposition au [tableau?]() -->

</details>

##

<details markdown="1">

<summary markdown="1">

## Logiciels de traitement de texte et de mise en page <a class="anchor" id="logiciels"></a>

</summary>

Dans plusieurs logiciels de traitement de texte (voir ci-dessous&#8239;: LibreOffice, Word, InDesign), en activant les ligatures, vos formes genrées seront automatiquement remplacées par des caractères post-binaires.

### Word et LibreOffice <a class="anchor" id="word"></a>
Dans Word, pour que les remplacements automatiques opèrent, il faut que les ligatures standards soient activées. Voici une vidéo qui montre comment faire&#8239;:

<iframe src="https://www.youtube.com/embed/275uFXtiQzU" title="How to enable OpenType features in Word 2016" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Dans LibreOffice, il n’y a rien de spécifique à activer, sélectionnez simplement la fonte post-binaire de votre choix dans l’outil texte. 

### Scribus <a class="anchor" id="scribus"></a>
Dans Scribus, il n’y a rien de spécifique à activer, sélectionnez simplement la fonte post-binaire de votre choix. 

### InDesign <a class="anchor" id="indesign"></a>
Pour que les caractères post-binaires apparaissent correctement, votre compositeur de paragraphe doit être réglé en compositeur universel.

• Voici comment faire via le panneau paragraphe&#8239;:
![panneau de l’outil paragraphe dans InDesign, avec un bouton menu entouré en haut à droite ](image.webp) ![menu déroulant avec les options de l’outil paragraphe dans InDesign, dans lequel l’option “compositeur de paragraphe universel Adobe” est sélectionné](image-1.webp)



• Voici comment faire en créant un style de paragraphe&#8239;:

![options de style de paragraphe dans InDesign, section justification. Dans le menu déroulant de l’entrée “Compositeur”, une flèche pointe l’option “compositeur de paragraphe universel Adobe”](image-2.webp)


Dans les autres logiciels de la suite Adobe (Illustrator, Photoshop…), il n’y a rien de spécifique à activer, sélectionnez simplement le style de la fonte post-binaire de votre choix dans l’outil texte.

</details>

##

<details markdown="1">

<summary markdown="1">

## Si vous êtes dessinateur·ice <a class="anchor" id="dessinateurice"></a>

</summary>

Si vous êtes dessinateur·ice de caractères et que vous voulez savoir comment rendre vos fontes post-binaires, rendez-vous sur la page [QUNI](/quni/) !

</details>

##

<details markdown="1">

<summary markdown="1">

## Quelques ressources et alliances <a class="anchor" id="ressources"></a>

</summary>

* [Bye Bye Binary sur Gitlab](https://gitlab.com/bye-bye-binary?target="_blank")
* [Testeur de differentes versions de fontes par Simon Bouvier](http://simon-bouvier.xyz/type-tester?target="_blank")
* [Testeur de fontes Velvetyne Type Foundry](http://velvetyne.fr/testing?target="_blank")
* [Inventaire des pratiques typographiques inclusives, non-binaires, post-binaires 2017-2021 par Camille Circlude](https://typo-inclusive.net/inventaire-des-pratiques-typographiques-inclusives-non-binaires-post-binaires-2017-2021?target="_blank")
* [L’ACADAM, grammaire non-binaire développée par Bye Bye Binary](https://genderfluid.space/documents/ACADAM_OK.pdf?target="_blank")
* [Bingo "J’aime pas l’écriture inclusive"](http://bingo.ttttoolbox.net?target="_blank")
* [Badass Libre Fonts by Womxn](https://www.design-research.be/by-womxn?target="_blank")
* [Club Maed](http://langage-inclusif-clubmed.fr?target="_blank")
* [Depatriarchise Design](https://depatriarchisedesign.com?target="_blank")
* [Un guide de Colophon foundry pour activer d’autres features OpenType](https://www.colophon-foundry.org/articles-guides/how-to-use-opentype-features?target="_blank")

</details>

</div>
