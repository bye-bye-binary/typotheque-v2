let positions = [];
let speeds = []; 
let angles = [];
let svgImages = [];
const numImages = 10;

function preload() {

    for (let i = 0; i < numImages; i++) {
        svgImages[i] = loadImage(`/assets/shurikens/asterisk-${i}.svg`);
    }
}

function setup() {
    let canvas = createCanvas(windowWidth, windowHeight);
    canvas.parent('home-screen');

    for (let i = 0; i < numImages; i++) {
        positions[i] = { x: random(width), y: random(height) };
        speeds[i] = { x: random(1, 3), y: random(1, 3) };
        angles[i] = 0;
    }
}

function draw() {
    clear();

    for (let i = 0; i < numImages; i++) {
        push();
        translate(positions[i].x, positions[i].y);
        rotate(angles[i]);

        let aspectRatio = svgImages[i].height / svgImages[i].width;
        let imgHeight = 105 * aspectRatio;

        image(svgImages[i], 0, 0, 105, imgHeight);

        positions[i].x += speeds[i].x;
        positions[i].y += speeds[i].y;
        angles[i] += 0.02;

        if ((positions[i].x + 20)> width || positions[i].x < 0) {
            speeds[i].x *= -1;
        }
        if (positions[i].y > height || positions[i].y < 0) {
            speeds[i].y *= -1;
        }
        pop(); 
    }

    checkCollisions();
}

function checkCollisions() {
    for (let i = 0; i < numImages; i++) {
        for (let j = i + 1; j < numImages; j++) {

            let dx = positions[j].x - positions[i].x;
            let dy = positions[j].y - positions[i].y;
            let distance = dist(positions[i].x, positions[i].y, positions[j].x, positions[j].y);
            let combinedSize = 50;

            if (distance < combinedSize) {
                print(`Images ${i} and ${j} are colliding!`);

                speeds[i].x *= -1;
                speeds[i].y *= -1;
                speeds[j].x *= -1;
                speeds[j].y *= -1;

                let overlap = combinedSize - distance;
                let normX = dx / distance;
                let normY = dy / distance;

                positions[i].x -= normX * overlap * 0.5;
                positions[i].y -= normY * overlap * 0.5;
                positions[j].x += normX * overlap * 0.5;
                positions[j].y += normY * overlap * 0.5;
            }
        }
    }
}

function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}