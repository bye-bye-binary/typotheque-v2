---
menu: À propos
---

<div id="sommaire-nav" markdown="1">

<div id="sommaire-content" markdown="1">

   * [Pratiques en commun de normes molles et&nbsp;rageuses](#pratiques)
   * [Des ligatures amies](#liga-amies)
   * [Une méthode commune](#methode)
   * [Faire un don](#don)
   * [Conditions matérielles d’existence de cette typothèque](#soutiens)

</div>

</div>

<div class="content" markdown="1">

# À propos

La typothèque Bye Bye Binary rassemble et diffuse une collection de caractères typographiques post-binaires réalisés au sein de la collective et au-delà, pour les usages du plus grand nombre. À la différence d’une fonderie dont la propriété est le socle, la collective BBB propose cet espace comme un lieu d’accueil et de diffusion pour ses adelphes dessinateur·ices de caractères désireux·ses de publier leurs fontes en communauté de pensée. BBB accompagne la finalisation de la «&#8239;qunification&#8239;» des fontes pour assurer la compatibilité, la présence et le bon fonctionnement des caractères post-binaires au sein de la fonte (voir page [QUNI](../03.quni/quni.fr.md)).
BBB n’a pas pour seul but le dessin de caractères mais, plus largement, d’alimenter le débat sur la charge politique du design graphique, du langage, des représentations des corps et des identités. [La collective Bye Bye Binary](https://genderfluid.space?target="_blank") adopte une position de recherche militante et communautaire, c’est-à-dire par et avec des personnes concernées, à travers un prisme féministe, queer, trans*, pédé·e, bi·e, gouin·e.

<details markdown="1">

<summary markdown="1">

## Pratiques en commun de normes molles et rageuses <a class="anchor" id="pratiques"></a>

</summary>

Les fontes de cette typothèque proposent de nombreux caractères post-binaires (lettres mutantes, [ligatures](#liga-amies), éléments de symbiose) en addition au point médian et autres solutions régulièrement utilisées pour écrire et composer des textes en inclusif. Nos claviers ne contiennent pas (encore) les touches qui correspondent à ces caractères. Alors pour rendre utilisable cet arc-en-ciel de signes par tous·tes, la collective Bye Bye Binary construit des pratiques en commun, des normes molles, rageuses et aux petits oignons. 

![exemple-baskervvol](image.webp)

</details>

##

<details markdown="1">

<summary markdown="1">

## Des ligatures amies <a class="anchor" id="liga-amies"></a>

</summary>

Parmi les alternatives au point médian, la collective a cuisiné les ligatures avec beaucoup de fondant.
En typographie, les ligatures désignent initialement des combinaisons de deux ou plusieurs caractères fusionnés pour des raisons esthétiques (ff, fi, ffl, …) ou linguistiques (æ, œ). Parce qu’elles sont fondées sur le lien et les transitions plus que sur la séparation, les ligatures sont un terrain de travail plein de promesses pour l’écriture inclusive. Créer de nouvelles associations de lettres (par exemple pour lier les terminaisons en «-if» et en «-ive» comme dans inclusif·ve) nécessite de créer de nouvelles cases dans la liste des caractères d’une fonte. Définie par le consortium international et industriel [Unicode](https://unicode.org?target="_blank"), cette liste prévoit pour chaque caractère un code permettant notamment le passage d’une fonte à une autre en maintenant la correspondance entre caractères. Cette liste est régulièrement mise à jour par Unicode, et donc susceptible d’accueillir de nouveaux caractères — un enjeu à venir pour la typographie post-binaire.

</details>

##

<details markdown="1">

<summary markdown="1">

## Une méthode commune <a class="anchor" id="methode"></a>

</summary>

Plusieurs fontes proposent déjà des ligatures et autres caractères post-binaires. La typothèque BBB a entrepris de les lister et les diffuser. 

Pour faciliter les travaux en cours et futurs, il nous a semblé important de construire une méthode commune pour harmoniser le fonctionnement technique des fontes post-binaires. Nous avons nommé cette méthode [læ Queer Unicode Initiative (QUNI)](../03.quni/). Un peu à la manière du Medieval Unicode Font Initiative, [MUFI](https://mufi.info?target="_blank"), projet visant à coordonner l’encodage et l’affichage de caractères médiévaux écrits en alphabet latin. 

Læ [QUNI](../03.quni/) permet de rassembler les fontes post-binaires, avec toute la diversité qu’elles contiennent, autour d’un même système d’encodage en vue de leur utilisation par un large public.

</details>

##

<details markdown="1">

<summary markdown="1">

## Faire un don <a class="anchor" id="don"></a>

</summary>

* [PayPal BBB](https://paypal.me/byebyebinary?target="_blank")
* Voir [CUTE (Conditions d’utilisation typographiques engageantes)](../licences/#dons) pour plus d’informations
<!-- Faire un lien direct vers l’ancre don -->

</details>

##

<details markdown="1">

<summary markdown="1">

## Conditions matérielles d’existence de cette typothèque <a class="anchor" id="soutiens"></a>

</summary>

L’histoire et les crédits de cette typothèque sont complexes, car ils embrassent des pratiques collectives. Cette généalogie se déploie sur plusieurs plusieurs années et à géométrie variable. Les crédits ne peuvent être réduit à une ligne. Il nous tient à cœur de rendre compte de cette réalité et des difficultés d’existence de ces recherches.  C’est grâce à la persévérance de notre communauté à trouver des cadres de financements qu’elles sont rendues possibles. Ceci témoignent de la précarité de nos pratiques. Merci de prendre soin de ce récit et de créditer correctement les personnes impliquées dans ces projets, ainsi que les dessinateur·ices des caractères typographiques.

En 2022, la collective BBB publie la première version de la Typothèque et du [QUNI](/quni/) grâce à une subvention (aide à la conception) de la [Fédération Wallonie-Bruxelles](https://www.federation-wallonie-bruxelles.be?target="_blank"), Commission des Arts Plastiques session Arts Numériques et le soutien d’[Open Source Publishing](https://osp.kitchen?target="_blank"). À l’initiative de Clara Sambot, accompagné·es de dessinateur·ices de caractères typographiques, se réunissent Eugénie Bidaut, Laura Conant, Camille Circlude, Loraine Furter, Laure Giletti, Pierre Huyghebaert, Enz@ Le Garrec, Quentin Lamouroux, Ludi Loiseau pour écrire à plusieurs mains en simple html, css et petits js la première version du QUNI (dont on doit le nom à une fulgurance de Félixe Kazi-Tani) et de la typothèque dont les contenus (tutos, images, textes) sont encore présents aujourd’hui.

Entre 2022 et 2024, plusieurs portions de subsides mises bout à bout permettent une série de sessions de travail autour de la question des licences. Une [bourse Vocatio](https://www.vocatio.be?target="_blank") ([Clara Sambot](https://www.instagram.com/clara_sambot?target="_blank")), une portion de subvention de la commission Arts Numériques (aide à la production) de la [Fédération Wallonie-Bruxelles](https://www.federation-wallonie-bruxelles.be?target="_blank") et le soutien du [Fond pour la recherche en art, FRArt](https://www.frs-fnrs.be/fr/l-actualite-fnrs/145-frart-fonds-pour-la-recherche-en-art?target="_blank") (FNRS) permettent la rédaction collective des [CUTE](/licences#CUTE/) (Conditions d’Utilisations Typographiques Engageantes) par Ludi Loiseau, Eugénie Bidaut, Mariel Nils, Clara Sambot, Camille Circlude, Enz@ Le Garrec, Laure Giletti, Pierre Huyghebaert avec le regard extérieur de Femke Snelting.

Une autre portion de la subvention de la commission Arts Numériques de la [Fédération Wallonie-Bruxelles](https://www.federation-wallonie-bruxelles.be?target="_blank") permet la poursuite du design et la mise en place d’un CMS pour la Typothèque V2 par Amélie Dumont, Laura Conant, Eugénie Bidaut et Ludi Loiseau&#8239;; l’augmentation du [QUNI](/quni/) par Camille Circlude, Eugénie Bidaut et Ludi Loiseau&#8239;; ainsi que le développement du Qunifieur en collaboration avec Thomas Bris.

Nous sommes toujours à la recherche d’autres soutiens pour la suite.

<div id="logos" markdown="1">![logo FWB](fwb.webp) ![logo FRArt](frart.webp) ![logo Vocatio](vocatio.webp)</div>

</details>

</div>

