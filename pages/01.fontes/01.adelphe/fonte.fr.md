---
title: Adelphe
sentence: 'Nos adelphes sont subversif·ves.'
font:
    author: 'Eugénie Bidaut '
    website: 'https://eugéniebidaut.eu/'
    date: '2024'
    license: CUTE
    download: 'https://gitlab.com/bye-bye-binary/adelphe/-/archive/main/adelphe-main.zip'
    link: 'https://gitlab.com/bye-bye-binary/adelphe'
    fontspecimen: 'https://xn--eugniebidaut-deb.eu/adelphe/'
fontsfiles:
    -
        style: 'Germinal Regular'
        font:
            'theme://css/fonts/Adelphe-GerminalRegular.woff':
                name: Adelphe-GerminalRegular.woff
                type: font/woff
                size: 33492
                path: 'theme://css/fonts/Adelphe-GerminalRegular.woff'
    -
        style: 'Germinal Italic'
        font:
            'theme://css/fonts/Adelphe-GerminalItalic.woff':
                name: Adelphe-GerminalItalic.woff
                type: font/woff
                size: 34728
                path: 'theme://css/fonts/Adelphe-GerminalItalic.woff'
    -
        style: 'Germinal Semi Bold'
        font:
            'theme://css/fonts/Adelphe-GerminalSemiBold.woff':
                name: Adelphe-GerminalSemiBold.woff
                type: font/woff
                size: 34784
                path: 'theme://css/fonts/Adelphe-GerminalSemiBold.woff'
    -
        style: 'Germinal Semi Bold Italic'
        font:
            'theme://css/fonts/Adelphe-GerminalSemiBoldItalic.woff':
                name: Adelphe-GerminalSemiBoldItalic.woff
                type: font/woff
                size: 35544
                path: 'theme://css/fonts/Adelphe-GerminalSemiBoldItalic.woff'
    -
        style: 'Germinal Bold'
        font:
            'theme://css/fonts/Adelphe-GerminalBold.woff':
                name: Adelphe-GerminalBold.woff
                type: font/woff
                size: 34360
                path: 'theme://css/fonts/Adelphe-GerminalBold.woff'
    -
        style: 'Germinal Bold Italic'
        font:
            'theme://css/fonts/Adelphe-GerminalBoldItalic.woff':
                name: Adelphe-GerminalBoldItalic.woff
                type: font/woff
                size: 35444
                path: 'theme://css/fonts/Adelphe-GerminalBoldItalic.woff'
    -
        style: 'Floréal Regular'
        font:
            'theme://css/fonts/Adelphe-FlorealRegular.woff':
                name: Adelphe-FlorealRegular.woff
                type: font/woff
                size: 34848
                path: 'theme://css/fonts/Adelphe-FlorealRegular.woff'
    -
        style: 'Floréal Italic'
        font:
            'theme://css/fonts/Adelphe-FlorealItalic.woff':
                name: Adelphe-FlorealItalic.woff
                type: font/woff
                size: 36064
                path: 'theme://css/fonts/Adelphe-FlorealItalic.woff'
    -
        style: 'Floréal Semi Bold'
        font:
            'theme://css/fonts/Adelphe-FructidorBold.woff':
                name: Adelphe-FructidorBold.woff
                type: font/woff
                size: 35384
                path: 'theme://css/fonts/Adelphe-FructidorBold.woff'
    -
        style: 'Floréal Semi Bold Italic'
        font:
            'theme://css/fonts/Adelphe-FlorealSemiBold.woff':
                name: Adelphe-FlorealSemiBold.woff
                type: font/woff
                size: 36140
                path: 'theme://css/fonts/Adelphe-FlorealSemiBold.woff'
    -
        style: 'Floréal Bold'
        font:
            'theme://css/fonts/Adelphe-FlorealBold.woff':
                name: Adelphe-FlorealBold.woff
                type: font/woff
                size: 35720
                path: 'theme://css/fonts/Adelphe-FlorealBold.woff'
    -
        style: 'Floréal Bold Italic'
        font:
            'theme://css/fonts/Adelphe-FlorealBoldItalic.woff':
                name: Adelphe-FlorealBoldItalic.woff
                type: font/woff
                size: 36756
                path: 'theme://css/fonts/Adelphe-FlorealBoldItalic.woff'
    -
        style: 'Fructidor Regular'
        font:
            'theme://css/fonts/Adelphe-FructidorRegular.woff':
                name: Adelphe-FructidorRegular.woff
                type: font/woff
                size: 34552
                path: 'theme://css/fonts/Adelphe-FructidorRegular.woff'
    -
        style: 'Fructidor Italic'
        font:
            'theme://css/fonts/Adelphe-FructidorItalic.woff':
                name: Adelphe-FructidorItalic.woff
                type: font/woff
                size: 35772
                path: 'theme://css/fonts/Adelphe-FructidorItalic.woff'
    -
        style: 'Fructidor Semi Bold'
        font:
            'theme://css/fonts/Adelphe-FructidorSemiBold.woff':
                name: Adelphe-FructidorSemiBold.woff
                type: font/woff
                size: 35844
                path: 'theme://css/fonts/Adelphe-FructidorSemiBold.woff'
    -
        style: 'Fructidor Semi Bold Italic'
        font:
            'theme://css/fonts/Adelphe-FructidorSemiBoldItalic.woff':
                name: Adelphe-FructidorSemiBoldItalic.woff
                type: font/woff
                size: 36588
                path: 'theme://css/fonts/Adelphe-FructidorSemiBoldItalic.woff'
    -
        style: 'Fructidor Bold'
        font:
            'theme://css/fonts/Adelphe-FlorealSemiBoldItalic.woff':
                name: Adelphe-FlorealSemiBoldItalic.woff
                type: font/woff
                size: 36908
                path: 'theme://css/fonts/Adelphe-FlorealSemiBoldItalic.woff'
    -
        style: 'Fructidor Bold Italic'
        font:
            'theme://css/fonts/Adelphe-FructidorBoldItalic.woff':
                name: Adelphe-FructidorBoldItalic.woff
                type: font/woff
                size: 36472
                path: 'theme://css/fonts/Adelphe-FructidorBoldItalic.woff'
published: true
publish_date: '05-06-2024 12:29'
---

L’Adelphe est un caractère de labeur dont l’enjeu principal est de proposer plusieurs manières de pratiquer l’écriture inclusive sur du texte long, en petit corps, et sans altération du gris typographique. Son nom, qui signifie à la fois frère et sœur de manière non-genrée, est très utilisé au sein des communautés militantes queers. Mais c’est aussi un mot qui trouve son étymologie dans le grec ancien, à la manière des mots savants. Et il y a une volonté avec ce caractère d’aller sur ce terrain, sur le terrain de la culture qui s’auto-définit comme «&#8239;haute&#8239;» et savante. Et, ainsi, de ne pas se laisser confisquer l’histoire de la langue et de l’écriture par le camp conservateur. C’est pourquoi l’Adelphe, dans son dessin, présente des proportions classiques, héritées de la Renaissance, et un tracé proche de la calligraphie, avec une fluidité dans le ductus qui permet de produire des formes harmonieuses, y compris dans le dessin des signes inclusifs. Il y a 3 versions de l’Adelphe qui proposent 3 manières différentes de pratiquer l’écriture inclusive. L’Adelphe Germinal dans lequel le point médian est utilisé, l’Adelphe Floréal dans lequel les premières lettres des terminaisons masculines et féminines sont marquées par des signes diacritiques souscrits (accents sous les lettres), et l’Adelphe Fructidor qui combine l’usage d’une forme alternative de « e » et de ligatures.