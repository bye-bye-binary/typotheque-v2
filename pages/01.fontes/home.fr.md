---
title: Fontes
---

La typothèque Bye Bye Binary rassemble et diffuse une collection de fontes post-binaires. BBB propose cet espace comme un lieu d’accueil et de diffusion pour ses adelphes dessinateur·ices de caractères désireux·ses de publier leurs fontes. 

Celles-ci sont téléchargeables librement mais leur usage répond à [un ensemble de conditions](../04.licences/) que nous vous invitons à consulter avant toute utilisation.