---
title: Nudles
fontsfiles:
    -
        style: Hairline
        font:
            'theme://css/fonts/Nudles-Hairline.woff':
                name: Nudles-Hairline.woff
                type: font/woff
                size: 78856
                path: 'theme://css/fonts/Nudles-Hairline.woff'
    -
        style: Thin
        font:
            'theme://css/fonts/Nudles-Thin.woff':
                name: Nudles-Thin.woff
                type: font/woff
                size: 78280
                path: 'theme://css/fonts/Nudles-Thin.woff'
    -
        style: Regular
        font:
            'theme://css/fonts/Nudles-Regular.woff':
                name: Nudles-Regular.woff
                type: font/woff
                size: 74112
                path: 'theme://css/fonts/Nudles-Regular.woff'
    -
        style: Bold
        font:
            'theme://css/fonts/Nudles-Bold.woff':
                name: Nudles-Bold.woff
                type: font/woff
                size: 78400
                path: 'theme://css/fonts/Nudles-Bold.woff'
    -
        style: Fat
        font:
            'theme://css/fonts/Nudles-Fat.woff':
                name: Nudles-Fat.woff
                type: font/woff
                size: 77920
                path: 'theme://css/fonts/Nudles-Fat.woff'
sentence: 'Exprimer votre cuisinier·e intérieur·e.'
font:
    author: 'Anne-Dauphine Borione & Max Lillo'
    website: 'https://daytonamess.com/'
    date: '2024'
    license: CUTE
    download: 'https://gitlab.com/bye-bye-binary/nudles/-/archive/main/nudles-main.zip'
    link: 'https://gitlab.com/bye-bye-binary/nudles#'
    fontspecimen: 'https://gitlab.com/bye-bye-binary/nudles/-/raw/main/Sp%C3%A9cimen%20Nudles.pdf?ref_type=heads&inline=false'
publish_date: '11-09-2024 17:10'
---

Nudles est une fonte open source ludique, frétillant·e, pas tout à fait monoline mais presque, avec beaucoup de personnalité. Inspiré·e par les formes des nouilles, Nudles donnera un sentiment de joie enfantine à vos créations, ou vous donnera juste un peu faim. De légères différences dans la position de la ligne de base, une légère inclinaison et des formes de caractères très uniques permettent un rythme particulier. Plusieurs graisses offrent une certaine marge de manœuvre pour exprimer votre cuisinier·e intérieur·e, que les ligatures inclusif·ves ne feront qu’accentuer. Un bol chaud de Nudles, ça vous tente ?