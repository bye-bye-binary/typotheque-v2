---
title: Escabeau
sentence: 'Une mécane bien costaud·e.'
font:
    author: 'Max Lilllo'
    website: 'https://maxlilllo.tumblr.com/'
    date: '2023'
    license: CUTE
    download: 'https://gitlab.com/maxlilllo/escabeau/-/archive/main/escabeau-main.zip'
    link: 'https://gitlab.com/maxlilllo/escabeau/-/tree/main?ref_type=heads'
    fontspecimen: 'https://gitlab.com/maxlilllo/escabeau/-/raw/main/specimen-Escabeau.pdf?ref_type=heads&inline=false'
fontsfiles:
    -
        style: Regular
        font:
            'theme://css/fonts/Escabeau.woff':
                name: Escabeau.woff
                type: font/woff
                size: 16772
                path: 'theme://css/fonts/Escabeau.woff'
publish_date: '10-06-2024 15:44'
---

Escabeau est une mécane bien costaud·e posé·e sur ses solides empattement intimement relié·e aux slogans militants. C'est un caractère full capital, là pour se faire entendre, porter, crier plus haut. Escbeau, à l’image du mouvement queer féministe, cherche la convergence des luttes et aborde ainsi des glyphes anti-capitalistes et aucun glyphe monétaire. I·els est open-source et voué·e à être abimé·e, coupé·e ou retracé·e à la main sur des pancartes.