<div id="sommaire-nav" markdown="1">

<div id="sommaire-content" markdown="1">

   * [CUTE (Conditions d’Utilisation Typographiques Engageantes)](#cute)
       * [Pourquoi ce texte ?](#pourquoi)
       * [Prise en mains](#prisenmains)
          <!-- * [Conditions pour utiliser, copier et partager](#utilisation) -->
           <!-- * Créditer, attribuer
           * Partager les dossiers complets
           * Se situer sur l'échelle des dons -->
         <!-- * [Conditions pour redistribution](#redistribution) -->
           <!-- * Garder les CUTE -->
         <!-- * [Conditions pour modification](#modification) -->
           <!-- * Ne pas supprimer les caractères post-binaires
           * Garder un lien avec le nom de la fonte -->
         <!-- * [Invitation à re-partager des modifications](#repartage) -->
       * [Dons](#dons)
         <!-- * [En situation de précarité](#précarite)
         * [En soutien à la recherche](#recherche)
         * [En situation de commande](#commande) -->
           <!-- * Pour une association
           * Pour une structure culturelle
           * Pour une institution culturelle
           * Pour une entreprise commerciale à taille humaine
           * Pour une grande entreprise -->
       * [Lexique](#lexique)
       * [Notes](#notes)
       * [Conditions matérielles d'écriture de ce texte](#conditions)
   * [SIL Open Font License](#sil)
   * [OIFL - Open Inclusif·ve Fonte Licence](#oifl)
   * [Creative Commons](#commons)

   </div>

</div>

<div class="content" markdown="1">

# Licences

Les fontes distribuées sur cette typothèque sont sous différentes licences et/ou conditions d’utilisation. En voici les détails :

<details markdown="1">

<summary markdown="1">

## CUTE (Conditions d’Utilisation Typographiques Engageantes) <a class="anchor" id="cute"></a>

</summary>

### Statut du texte

Les conditions d'utilisations typographiques engageantes (les CUTE), écrites par la collective Bye Bye Binary, sont une mise en relation entre les personnes qui dessinent, diffusent et utilisent des fontes post-binaires. Ces conditions sont une sorte de contrat, un guide d'usage, utilisables par toute personne souhaitant publier une fonte post-binaire. À la manière d'une licence, d’une moule sur son rocher, ce texte voyage, se diffuse avec les fichiers de fontes lors de leur téléchargement*.* Elles se détachent de la plupart des licences libres en intégrant la question de l'économie et des conditions matérielles d'existence des dessinateur·ices. Les idées critiques et politiques qui les animent pollinisent ainsi des pratiques graphiques engagées dans une perspective radicalement féministe, antiraciste, anticapitaliste, *queer* et *trans*, pédé·e, bi·e, gouin·e.

### Pourquoi ce texte ? <a class="anchor" id="pourquoi"></a>

Depuis 2018, la collective Bye Bye Binary travaille activement au dessin de fontes post-binaires pour permettre à la langue de dépasser les débats autour de l'écriture inclusive. En 2022, elle publie la première version d'une typothèque. Les caractères diffusés le sont alors sous licences libres (OFL[🏀](#🏀) , OIFL[🌰](#🌰) ou CC-BY-NC-SA [🍏](#🍏)). Leurs conditions très permissives ainsi que l'héritage et l'approche universalistes ne prennent pas en compte les dynamiques de pouvoir et inégalités économiques structurelles.



   * *Exemple : Une institution importante utilise les caractères sans donation en adéquation avec leurs moyens, alors que des étudiant·es souvent précaires contribuent financièrement à la recherche ?*

Après un premier geste de réécriture de l'OFL en langage inclusif, l'OIFL, il est apparu nécessaire à la collective BBB d'écrire des conditions d'utilisations spécifiques aux fontes post-binaires. Les pratiques de partage et l'invitation à modification des licences libres [😋](#😋) est cependant une dimension qui est chère à la collective et qui est conservée dans ces CUTE. Dès que ça circule, ça bouge.

Ces conditions d'utilisation cherchent à naviguer entre les « tensions et paradoxes des politiques de partage » [🐚](#🐚) et la critique de la notion d'autorat. Premièrement, la figure de l’auteur·ice original·e est ambigüe, toute production est infusée des précédentes et s'inscrit dans un continuum de références et de pensée. Deuxièmement, le fait d’apposer une signature sur une production est liée à une histoire d'appropriations de pratiques non-canonisées et négligées par l'histoire dominante. C'est donc une question dont il faut prendre soin, en faisant attention aux références, aux généalogies, aux contextes. Il s'agit « d'éviter l’appropriation abusive en insistant sur une attribution inclusive. » [🐝](#🐝).

Par la rédaction de ces conditions comme outils de résistance, Bye Bye Binary indique un premier cadre d'utilisation pour les utilisateur·ices et permet aux dessinateur·ices de publier leurs fontes selon un rapport de force moins déséquilibré face aux logiques propriétaires et de résister collectivement aux dynamiques d'extraction et de colonisation des savoirs [🌾](#🌾).

Ces conditions d'utilisation s'inscrivent dans le champ de la typographie et mobilisent plusieurs points d'attention:

   * Le fait que la production de formes typographiques s'inscrit dans une longue histoire de la copie.[lexique]
   * La circulation des objets typographiques sur les plateformes numériques a tendance à rompre les liens et à rendre notre écosystème moins durable. [🤖](#🤖)
   * La longue histoire d’invisibilisation des personnes minorisées dans le champ de la typographie [🎁](#🎁).
   * La difficulté de garder traces des personnes au sein de pratiques collectives.
   * L'attention aux sources, car référencer permet de ne pas perdre des existences. En l'occurence celle des *messy histories *[👁](#👁) des luttes LGBTQIA+.


### Prise en mains <a class="anchor" id="prisenmains"></a>

Les Conditions d'utilisation de BBB invitent à utiliser, copier, partager, redistribuer et modifier les fontes publiées sous les CUTE, et re-partager les versions modifiées, y compris pour des usages commerciaux.


#### Conditions pour utiliser, copier et partager <a id="utilisation"></a>
* Toute personne ou institution ayant pris connaissance de ces conditions d'utilisation est en mesure de télécharger, installer et utiliser les caractères dans tout logiciel permettant la mise en forme de texte.
* Ces conditions d'utilisation sont propres aux fontes et non aux documents qu'elles servent à produire (voir: contamination).

* *Exemple: Un livre qui utilise une fonte sous CUTE n'est pas régi par ces conditions d'utilisation.*

##### Créditer, attribuer
* Il est nécessaire de créditer læ ou les dessinateur·ices de la fonte ainsi que de mettre un lien vers la source de publication original (la typothèque BBB, ou autre), permettant ainsi de relier au contexte de travail. 

##### Partager les dossiers complets
* La fonte est un logiciel composé d'éléments de dessins vectoriels, mais aussi de lignes de code décrivant son comportement et de documentation permettant de comprendre son contexte de création.[lexique: paquet] Pour le partage et la transmission des savoirs, ce sont tous ces ingrédients qui ont intérêt à voyager avec le fichier de fonte. En ce sens, pour chaque copie, partage ou redistribution, il est important de bien prendre soin de l'ensemble.

##### Se situer sur l'échelle des dons
* Voir la section "Dons" pour les critères de cette condition importante.


#### Conditions pour redistribution <a id="redistribution"></a>
* Les fontes peuvent être redistribuées sur des sites internet, cloud, de pair à pair, et n'importe quel support informatique. Dans ce cas, les conditions précédentes *Créditer, attribuer* et *Partager les dossiers complets *s'appliquent.

     * *Exemple : Vous administrez un catalogue de fontes en ligne, merci de publier un zip complet et non un seul fichier .otf*

##### Garder les CUTE
* Si vous redistribuez ou modifiez une fonte publiée sous CUTE, elle doit rester sous CUTE. Elle peut aussi cohabiter avec d'autres licences, guides et conditions d'utilisation dans le même esprit comme CC4R, GenderFail, ACAB ou les Non Violent Licenses. Cette condition produit une viralité qui permet aux fontes de polliniser les pratiques graphiques engagées. Et dans le même temps, elle empêche toute personne de s'approprier le travail et sa p·maternité.

* Les CUTE héritent de la culture du Free, du Libre et de l'Open Source, mais elles ne sont techniquement compatibles avec aucune licence FLOSS à ce jour. Les CUTE adoptent une position radicalement et volontairement incohérente avec celles-ci dans leur état actuel.

     * *Exemple : il y a des caractères post-binaires dans ma fonte donc j'utilise ces conditions d'utilisations, mais j'aimerais la fermer aux pratiques militaires, alors je la fais cohabiter avec la Non Violent Public License [🔫](#🔫) et c'est OK.*

#### Conditions pour modification <a id="modification"></a>
   * Les fontes sous les CUTE ont été programmées avec læ QUNI (Queer Unicode Initiative)[lexique], des fonctionnalités d'activation des caractères post-binaires, une recette, dont vous êtes invité·es à prendre connaissance avant toute modification ou fork [lexique]. Les conditions de modification s'ajoutent au conditions précédentes :


##### Ne pas supprimer les caractères post-binaires
* Les caractères post-binaires peuvent être modifiés et augmentés comme le reste du fichier. Mais, parce que ces caractères font partie intégrante de la fonte, ils ne peuvent être supprimés. Ceci vaut également pour les fonctionnalités OpenType qui les activent. Toute copie ou version modifiée doit garder le set complet.

##### Renommer mais faire lien
* Pour assumer les filiations et rendre plus lisibles les chemins, les forks doivent changer le nom de la fonte en ajoutant un complément au nom original. Pour toute question ou hésitation, contactez les auteur·ices de départ.

    * *Exemple : Adelphe qui devient Adelphe-Sylex. Et pour un second fork, Adelphe-Sylex devient Adelphe-Sylex-Galette*

* En ce qui concerne les fontes ayant le préfixe BBB, garder le nom de la fonte de départ sans le BBB et y ajouter un complément. En effet, les initiales BBB sont un identifiant pour les fontes dessinées par la collective, mais il ne s'agit pas de les imposer comme une signature. C'est aussi l'occasion de marquer le passage de relais pour la maintenance et le suivi du projet, qui passe alors aux mains des dessinateur·ices du fork.

    *Exemple : BBB-Baskervvol qui devient Baskervvol-Sylex.*


#### Invitation à re-partager les modifications <a id="repartage"></a>
* Dans le cas d'élargissement de la palette de caractères (*glyphset* [lexique]) sans modifications des glyphes existants, les auteur·ices qui ont publié la fonte sous ces conditions seraient heureux·ses de pouvoir les incorporer à sa version de base sans changement de nom. La·e dessinateur·ice de l'augmentation serait alors ajouté·e à la liste des dessinateur·ices de la fonte.

    * *Exemple : L'ajout d'accents et diacritques pour l'écriture du vietnamien.*

* À chaque modification, les nouvelle·aux dessinateur·ices sont ajouté·es à la liste des dessinateur·ices précédent·es dans les *font infos* [lexique]. L'historique complet est conservé, idéalement avec les dates et contextes dans un fichier *font log* [lexique].


### Dons <a class="anchor" id="dons"></a>

Les fontes distribuées sous ces conditions d'utilisation ont été dessinées par et pour des personnes qui sont solidaires des luttes contre le cis-hétéro-patriarcat, la suprématie blanche, le validisme et le capitalisme. L'utilisation de fontes post-binaires ne saurait évidemment pas se substituer à la mise en place d'autres actions de militance contre ces systèmes d'oppression. 

Ces conditions d'utilisation englobent la question de l'économie vertueuse de la recherche et l'adoption d'une position féministe matérialiste [😈](#😈). Recevoir des dons permet de créer les conditions matérielles d'existence pour les chercheur·ses actif·ves dans le champ de la typographie post-binaire. En effet, un soutien financier rend possible la participation de personnes ne pouvant pas se permettre du travail non-rémunéré, encourage les dessinateur·ices à publier plus, et ouvre à une plus grande variété esthétique et politique. Ce domaine de recherche est précaire, il existe sur un fil grâce à quelques subventions et commandes ponctuelles. Intégrer une échelle de donations aux conditions d'utilisation des fontes permet d'insister sur les besoins concrets aux différents endroits de l'écosystème. Ce texte prend ainsi de la distance avec l'idée reçue selon laquelle « libre » équivaut à « gratuit ».

Les cas listés ci-dessous sont des exemples non-exhaustifs car cette échelle de valeur monétaire proposée est située culturellement dans un contexte belgo-français. À vous de vous projeter et d'évaluer votre propre position au sein des dynamiques de pouvoir. L'outil *Green bottle sliding scale *[🧪](#🧪) peut être un outil complémentaire utile pour vous auto-déterminer et responsabiliser vos choix.

En ce qui concerne les donations reçues par BBB, elles ne permettent pas de financer des salaires. Une partie est reversée aux dessinateur·ices externes. Le reste permet le regroupement de la collective et est utilisé principalement pour des billets de train.



#### En situation de précarité
   * *Vous voulez utiliser des fontes post-binaires pour un projet étudiant, un fanzine, une petite annonce pour votre coloc, un tract TPBG ou peindre des ligatures sur des banderoles de manifestation. Foncez !*
   * **Participez à la hauteur des moyens du projet, gardez votre argent si c'est un frein.**

#### En soutien à la lutte et la recherche
   * *Vous utilisez des fontes post-binaires pour la rédaction de vos propres travaux (intégrées à votre œuvre, sous-titres de film, écrits politiques, dossier médiatique, thèse universitaire…), au sein de pratiques collectives (associatives autogérées non financées, lieux précaires, militantes…). Vous souhaitez soutenir la recherche et voir proliférer les fontes post-binaires.*
   * **Don entre 10€ et 50€**

#### En situation de commande
   * *Vous être graphiste et souhaitez utiliser des fontes post-binaires dans le cadre d'une commande de design graphique, responsabiliser votre commanditaire à l'économie vertueuse est encouragé. Rapportez-vous à l'échelon de donation correspondant au statut de votre commanditaire pour lui proposer un montant en adéquation avec son ampleur et le cadre de diffusion du projet.*
   * **Don entre 50€ et 1000€**

##### Pour une association
   * *Vous avez un petit budget alloué à la communication de votre structure ou d'un évènement que vous organisez. Vous faites appel aux services d'un·e graphiste, d'un·e imprimeur·se. Vous êtes subsidié·es, vous ne bénéficiez pas de rentrée d'argent d'autre nature (billets d'entrée ou bénéfice d'un bar).*
   * **Don entre 50€ et 150€**

##### Pour une structure culturelle
   * *Vous êtes un théâtre, une galerie, une salle de concert, un festival. Vous avez un budget dédié à la production de vos supports de communication, voire un·e chargé·e de communication et/ou un·e graphiste salarié·e au sein de votre structure. En plus de subventions, vous bénéficiez de rentrées d'argent complémentaires (billets d'entrée ou bénéfice d'un bar).*
   * **Don entre 50€ et 500€**

##### Pour une entreprise commerciale locale
   * *Vous êtes une épicerie coopérative, une friperie, une boulangerie (qui pétrit des bretz-iels), une librairie féministe, le seul bar lesbien de la ville, une maison d'édition indépendante, etc. qui se démène tant bien que mal avec les logiques capitalistes et tente de changer le monde à son échelle.*
   * **Don entre 50€ et 500€**

##### Pour une institution culturelle
   * *Vous êtes un musée, une école supérieure, une scène nationale, une galerie, etc. Vous avez un département communication au sein de votre structure. En plus de soutiens et subventions, vous bénéficiez de rentrées d'argent complémentaires (billets d'entrée ou bénéfice d'un bar).*
   * **Don entre 300€ et 1000€**

##### Pour une grande entreprise
   * *Vous souhaitez produire des objets à grande échelle avec des fontes post-binaires ou vous souhaitez vous donner une image inclusive et pinkwasher vos insuffisances structurelles.*
   * **Allez voir ailleurs**


### Lexique <a class="anchor" id="lexique"></a>

* **Contamination :** Dans les Open Font Licences telles que l'OIFL [🌰](#🌰), on trouve la formulation suivante : « L'obligation pour les fontes de rester sous cette licence ne s'applique pas à tout document créé à l'aide des fontes ou de leurs dérivés ». Les Open Font Licences utilisent cette exception spécifique pour éviter qu'une fonte modifiée ou distribuée ne « contamine » des sites web, des livres, des affiches et d'autres documents composés avec cette fonte.


* **Copie :** Les typographes ne travaillent jamais à partir de rien.** **Quelques exemples:

   * Stéphane Darricau, *De quoi Garamond est-il le nom ?*, 2023.
   * [bureaubrut.com/product/de-quoi-garamond-est-il-le-nom/](https://bureaubrut.com/product/de-quoi-garamond-est-il-le-nom?target="_blank")
   * *10,000 Original Copies*, une conférence Kris Sowersby (TypeCon 2018 XX) autour des concepts européens d'originalité et de paternité [klim.co.nz/blog/10000-original-copies](https://klim.co.nz/blog/10000-original-copies?target="_blank")
   * OSP, *Fluxisch-Else*, 2017 [osp.kitchen/foundry/fluxisch-else/](http://osp.kitchen/foundry/fluxisch-else?target="_blank")
   * Généalogie du Baskervvol :
   * — [John Baskerville, Birmingham, c. 1750.]
   * — Claude Jacob, Strasbourg, 1784.
   * — ANRT (Alexis Faudot, Rémi Forte, Morgane Pierson, Rafael Ribas, Tanguy Vanlaeys, Rosalie Wagner), Nancy, 2017-2018.
   * — BBB Baskervvol (Eugénie Bidaut, Julie Colas, Camille Circlude, Louis Garrido, Enz@ Le Garrec, Ludi Loiseau, Édouard Nazé, Julie Patard, Marouchka Payen, Mathilde Quentin), Bruxelles 2018-2022. Ajout de glyphes non-binaires.
   * — BBB Baskervvol medium, semibold et bold (Rosalie Wagner et Thomas Huot-Marchand), Nancy 2024.

* **Font info :** La fenêtre Infos fonte est omniprésente dans les éditeurs de polices. C'est une boîte de dialogue qui permet de spécifier et compléter les meta données de la fonte définissant par exemple le nom de la graisse, son type de courbe ou le nom de ses différents masters. On trouve aussi au sein des champs de cette boîte de dialogue une entrée intitulée *copyright*.

* **Fonte :** désigne l'ensemble des fichiers publiés par le(s) porteureuses du projet de caractère. Cela comprend l’ensemble des représentations visuelles des caractères (glyphes), les fichiers sources, la documentation, le FONTLOG ainsi que ce texte de conditions d'usage. Le terme fonte, dérivé de l’anglais font, désigne l’ensemble des représentations visuelles de caractères (glyphes), comprenant des lettres, des chiffres, des signes de ponctuation et des symboles, qui sont utilisés pour composer du texte. Son étymologie provient des caractères en plomb qui étaient fondus dans des fonderies typographiques. Le terme « fonte » est préféré à « police de caractères » pour éviter la répétition du mot polysémique « police ». → voir *ACAB *[genderfluid.space/lexiquni.html](https://genderfluid.space/lexiquni.html?target="_blank")

* **Fork** ou « Version modifiée » désigne tout dérivé réalisé en ajoutant, supprimant ou remplaçant - en partie ou en totalité - l'un des composants de la version publiée. Un fork, en français « fourche » ou « embranchement », désigne un dérivé d’une fonte existante qui partage le même dessin initial augmenté par exemple de nouveaux glyphes, de fonctionnalités OpenType, de variations de dessins ou de scripts additionnels. Ce terme tire son origine de l’informatique, où il désigne un logiciel dont le code source découle d’un logiciel précédent.

* **FONTLOG** désigne un document texte qui vise à archiver et répertorier les versions, détail des ajouts successifves à un caractère au fil du temps. Le FONTLOG contient aussi un sommaire des fichiers contenus dans le dossier.

* **Glyphset :** Le *glyphset* est l'ensemble de des caractères couverts par une fonte donnée. Il se définit souvent en termes de plages Unicode (par exemple: "Latin étendu A" ou "Grec et copte").

* **Police** → voir *Fonte*

* **Paquet :** Pour un souci de lisibilité, ce texte décrit les conditions de partage des fontes en tant que dossiers plutôt que comme un simple fichier .otf ou .ttf. Nous ajoutons ici une entrée pour le mot «paquet» parce que nous souhaitons garder un lien avec la pratique du *package* qui correspond à notre souci de favoriser l'épanouissement des écosystèmes. L'empaquetage est une méthode fondamentale du logiciel libre qui consiste à préparer soigneusement un logiciel ou une police numérique avant de le publier. En s'assurant que toutes les ressources nécessaires soient incluses ou référencées, les développeur·euses préparent un environnement dans lequel le logiciel peut-être déployé. Dans le cas d'une fonte digitale cela signifie de prendre soin d'inclure le texte de licences ou de conditions, la documentation, le FONTLOG, les fichiers sources et les infos fontes.

* **QUNI** rend utilisable les fontes post-binaire. Des pratiques en commun, des normes molles, rageuses et aux petits oignons, forment ensemble læ Queer Unicode Initiative (QUNI). Le QUNI permet de rassembler les fontes avec toute la diversité qu'elles contiennent, autour d'un même système d'encodage en vue de leur utilisation par un large public. Voir plus en détails [typotheque.genderfluid.space/quni.html](https://typotheque.genderfluid.space/quni.html?target="_blank")

* **Unicode :** Unicode est un standard informatique qui permet des échanges de textes dans différentes langues, à un niveau mondial. Il est développé par le Consortium Unicode, qui réunit les grands intérêts privés du secteur numérique mondial et vise au codage de texte écrit en donnant à tout caractère de n'importe quel système d'écriture un nom et un identifiant numérique, et ce de manière unifiée, quels que soient la plate-forme informatique ou le logiciel utilisé.


### Notes <a class="anchor" id="notes"></a>

**🏀 La *SIL Open Font License* (OFL)** est à la fois une *free software license (en)* et une *open-source license (en)* conçue par l'organisation évangéliste américaine SIL International dans le but de distribuer plusieurs de ses polices de caractères Unicode, incluant Gentium Plus, Charis SIL et Andika. L’*Open Font License* a été publiée pour la première fois en novembre 2005.
[openfontlicense.org/open-font-license-official-text/](https://openfontlicense.org/open-font-license-official-text?target="_blank")

**🌰 L'OIFL** a été rédigée par BBB en 2021 comme une première tentative de débinariser la langue du texte qui accompagnait la plupart des fontes forkée par la collective. [typotheque.genderfluid.space/licences.html](https://typotheque.genderfluid.space/licences.html?target="_blank")

**🍏 CC-BY-NC-SA** les licences Creative Commons constituent un ensemble de licences régissant les conditions de réutilisation et de distribution d'œuvres. Élaborées par l'organisation Creative Commons, elles ont été publiées pour la première fois le 16 décembre 2002. La CC-BY-NC-SA correspond à Creative Commons — Attribution — Pas d’Utilisation Commerciale — Partage dans les Mêmes Conditions.
[creativecommons.org/licenses/by-nc-sa/2.0/be/deed.fr](https://creativecommons.org/licenses/by-nc-sa/2.0/be/deed.fr?target="_blank")

**😋** Nous faisons ici référence aux pratiques qui sont parfois aussi appelées Open Source, FLOSS, Copyleft, culture du libre.

**🐝 CC4r – Conditions collective de réutilisation qui** ont nourri et inspiré la collective BBB pour l'écriture des présentes conditions. La CC4r a été développée pendant la session de travail Constant Unbound Libraries (printemps 2020) et a fait suite aux discussions et contributions à la journée d’étude Auteur·es du futur (automne 2019). Elle est basée sur la Free Art License et inspirée par d’autres projets de licences tels que the (Cooperative) Non-Violent Public License et the Decolonial Media license. [constantvzw.org/wefts/cc4r.fr.html](https://constantvzw.org/wefts/cc4r.fr.html?target="_blank")

**🐚** Citation de "Mes Lettres", conférence de Femke Snelting pendant le séminaire *Post $cript. Écoles sous licence. Typographie : usages, économie, fichiers, licences* à l'école de recherche graphique, Bruxelles, mars 2023.

**👁** — Scotford, Martha, « Toward an Expanded view of Women in Graphic Design. Messy History vs Neat History », *Visible Language*, no 28, 1994, p. 368-388. 
Dans son texte, Martha Scotford développe la notion de *messy history* [histoire mal rangée] en opposition à la *neat history *[histoire bien rangée] : « La* neat history* est l'histoire conventionnelle : elle se concentre sur les activités traditionnelles et le travail d'individus, souvent des hommes designers. La *messy history* cherche à découvrir, étudier et inclure la variété des approches et activités alternatives, qui font souvent partie de la vie professionnelle des femmes designers.

— Canlı, Ece, « Design History Interrupted. A Queer-Feminist Perspective », dans *The Responsible Object. A History of Design Ideology for the Future*, Amsterdam, Valiz, Melbourne, Uberschwarz, 2016, p. 187-198.

Dans son texte, Ece Canlı actualise cette notion dans une perspective* queer*-féministe, « targeting greater power strucure », en allant au-delà de « l'historiographie moderniste » qui consiste à aligner les accomplissements des femmes à celles des hommes. 

**🎁** "In the governance of typography we have a very long history of prioritizing white men as cult heroes. We establish his legacy, worship and emulate him, award prizes, and evolve entire industries in and around his power" Soulellis, Paul. *What Is Queer Typography?* Queer.Archive.Work, 2021.

**🔫 Non Violent Public Licenses** Les Nonviolent Public Licenses visent à assurer une protection de base contre les formes de violence, de coercition et de discrimination dont les créations sont souvent l'objet dans le monde moderne. Ces licences couvrent plusieurs formats d'œuvres créatives, mais comportent des conditions supplémentaires pour les logiciels, étant donné le pouvoir qu'ils ont en tant qu'outils en dehors de leurs dimensions créatives.
[thufie.lain.haus/NPL.html](https://thufie.lain.haus/NPL.html?target="_blank")

**🧪 Green bottle sliding scale** La *green bottle sliding scale* est un modèle d'échelle mobile qui tient compte des différentes expériences financières. Développée par Alexis J. Cunningfolk, praticien en guérison communautaire, la *green bottle sliding scale* est un outil de justice économique qui permet aux participants d'ajuster le paiement en fonction de l'accès aux ressources.
[Green bottle sliding scale](https://images.squarespace-cdn.com/content/v1/54a1bf90e4b07c077787ed68/1440108759301-1YFJ9LY0JNJVAKRJZ0AI/image-asset.png?format=2500w?target="_blank")

**😈 Féminisme matérialiste** Le féminisme matérialiste est une branche du féminisme qui se caractérise par l'usage d'outils conceptuels et d'une grille de lecture issues du marxisme. Elle porte donc une attention particulière aux conditions matérielles d'existence des groupes sociaux induites au sein de la société patriarcale.

**🌾 Dynamiques d'extraction et de colonisation des savoirs** Ces différentes notions ici accolées invitent à s'interroger sur les conditions de production de savoirs situés, queer, décoloniaux, trans*féministes ou intersectionnels dans un système capitaliste, sexiste, bourgeois et raciste qui invisibilise et disqualifie l'existence de ces contre-récits. Il s'agit de s'interroger et de rester critique des gestes (et de ses propres gestes) coloniaux et extractivistes, lorsque ces mêmes discours et savoirs contre-hégémoniques sont déplacés et propulsés dans des logiques de capitalisation qui rompent les généalogies sans aucune éthique du soin possible pour les communautés qui s’affairent à ces savoirs.

**** Le caractère numérique des pratiques typographiques génèrent des modes de circulations spécifiques. La culture du logiciel libre essaie depuis des dizaines d'années de prendre soin et de faciliter les modes de circulation des objets numériques. Depuis moins longtemps, les plateformes ont basé la massification de leurs profits en s'appuyant sur cette liberté de circulation sans soutenir les conditions culturelles de production des savoirs. 


### Conditions matérielles d'écriture de ce texte <a class="anchor" id="conditions"></a>

En 2022, la collective BBB a publié la première version de la typothèque grâce à une subvention de la Fédération Wallonie-Bruxelles, Commission des Arts Plastiques session Arts Numériques.
 
En 2022 et 2023, une série de sessions de travail autour de la question des licences a été soutenue par la bourse Vocatio, par l’intermédiaire de Clara Sambot.

En 2023 et 2024, deux portions de subsides mises bout à bout, la première émanant de la commission Arts Numériques de la Fédération Wallonie-Bruxelles et la seconde du FRArt (FNRS) ont permis la rédaction collective de ce texte.

L’écriture de ces conditions a été faite à plusieurs mains et en invite d’autres. Cette première version de janvier 2024 est amenée à évoluer avec la pratique, les retours et les contributions. En publiant le texte de ces conditions sous licence CC4r, nous encourageons encore son déploiement dans d’autres versions.

<!-- FIN CUTE -->
</details>

##

<details markdown="1">

<summary markdown="1">

## SIL Open Font License <a class="anchor" id="sil"></a>

</summary>

[Licence SIL OFL](https://openfontlicense.org/open-font-license-official-text?target="_blank")

</details>

##

<details markdown="1">

<summary markdown="1">

## OIFL - Open Inclusif·ve Fonte Licence <a class="anchor" id="oifl"></a>

</summary>

### PRÉAMBULE : 
Le but de la licence OIFL est de stimuler le développement collaboratif de fontes à travers le monde, d’aider à la création de fontes dans les communautés académiques ou linguistiques, et de fournir un cadre libre et ouvert à travers lequel ces fontes peuvent être partagées et améliorées de manière collective.

Les fontes sous licence OIFL peuvent être utilisées, étudiées, modifiées et redistribuées librement, tant qu’elles ne sont pas vendues par elles-même. Les fontes, ainsi que tous leurs dérivés, peuvent être fournies avec, incorporées, redistribuées et/ou vendues avec des logiciels quelconques, sous réserve que les appellations réservées ne soient pas utilisées dans les produits dérivés. La licence des fontes et de leurs produits dérivés ne peut cependant pas être modifiée. Cette obligation de conserver la licence OIFL pour les fontes et leurs produits dérivés ne s’applique pas aux documents crées avec.

### DÉFINITIONS :
* L’appellation « fonte informatique » désigne l’ensemble des fichiers mis à disposition par le(s) titulaire(s) du copyright sous cette licence et clairement identifiés comme tels. Cela peut inclure des sources, des scripts de génération, de la documentation.
* L’ appellation ‘nom de fonte réservé’ désigne n’importe quel nom désigné comme tel dans le copyright. ‘Version originale’ désigne une ‘fonte informatique’ telle que distribuée par le(s) titulaire(s) du copyright.
* ‘Version modifiée’ désigne tout travail dérivé obtenu en ajoutan SIL Open Font Licence, supprimant ou remplaçant tout ou partie de n’importe quel élément de la ‘version originale’, en changeant de format ou en portant la ‘fonte informatique’ vers un nouvel environnement.
* ‘l’Auteueur·ice’ désigne un·e créateur·ice,

### PERMISSIONS ET CONDITIONS :
La permission est ici accordée, sans contrepartie, à toute personne ayant obtenu une copie de la ‘fonte informatique’ de l’utiliser, l’étudier, la copier, la fusionner, l’inclure, la modifier, la redistribuer et la vendre, modifiée ou non, aux conditions suivantes :
* La ‘fonte informatique’, en tout ou partie, en version originale ou modifiée, ne peut être vendue par elle même.
* Les versions originales ou modifiées de la ‘fonte informatique’ peuvent être liées, redistribuées et/ou vendues avec n’importe quel logiciel, à condition que chaque copie contienne la déclaration de copyright ci-dessus ainsi que cette licence.
* Ces pièces peuvent être soit des fichiers textes autonomes, des entêtes de fichiers accessibles à une lecteur..ice humain..e, ou figurer dans des méta-données accessibles par programme, dans des fichiers textes ou binaires, tant que leur texte est facilement accessible à un..e utilisateueur..ice.
* Aucune version modifiée de la ‘fonte informatique’ ne peut utiliser les noms réservés sans autorisation écrite expresse du..dela propriétaire du copyright. Cette restriction ne s’applique qu’au nom principal de la police tel que présenté à l’utilisateur..ice.
* Le ou les noms un..e titulaire du copyright et ceux du ou des auteur..ices ne peuvent pas être utilisés pour promouvoir, conseiller ou faire de la publicité pour des versions modifiées, sauf pour rappeler leur(s) contribution(s), ou alors avec leur autorisation écrite expresse.
* La ‘fonte informatique’, modifiée ou non, en tout ou partie, doit être distribuée avec exactement cette même licence. Cette obligation de conserver la licence ne s’applique pas aux documents crées en utilisant la ‘fonte informatique’.

### FIN DE CONTRAT :
Cette licence devient nulle et sans objet si l’une quelconque des conditions ci dessus n’est pas remplie.

### CLAUSE DE NON RESPONSABILITÉ :
La fonte informatique est fournie telle quelle, sans garantie d’aucune sorte, ni implicite ni explicite, y compris et de manière non limitative sans garantie de commerciabilité, de conformité à une utilisation particulière, de respect des copyrights, brevets, marques commerciales ou autres droits. En aucun cas la..e titulaire du copyright ne pourra être tenu pour responsable en cas de réclamation, dommage ou autre conséquence, y compris aucun dommage général, spécifique, indirect, incident ou résultant, que ce soit contractuellement, à titre de dédommagement ou autrement, survenant suite à l’usage ou l’impossibilité d’usage de la fonte informatique, ou toute autre affaire impliquant la fonte informatique.

– Pierre Hanser, ré-écriture par Bye Bye Binary.

</details>

##

<details markdown="1">

<summary markdown="1">

## Creative Commons <a class="anchor" id="commons"></a>

</summary>
[Licences Creative Commons](https://creativecommons.org/share-your-work/cclicenses?target="_blank")

</details>

</div>