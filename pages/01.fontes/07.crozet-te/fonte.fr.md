---
title: Crozet·te
sentence: 'La·e Crozet·te est un·e caractère inclusif·ve et fleuri·e.'
font:
    author: 'Thaïs Cuny'
    website: 'https://www.instagram.com/thais.cy'
    date: '2024'
    license: CUTE
    download: 'https://gitlab.com/bye-bye-binary/crozet-te/-/archive/main/crozet-te-main.zip'
    link: 'https://gitlab.com/bye-bye-binary/crozet-te#'
    fontspecimen: 'https://gitlab.com/bye-bye-binary/crozet-te/-/raw/main/CROZET%C2%B7TE_Specimen.pdf?ref_type=heads&inline=false'
fontsfiles:
    -
        style: Regular
        font:
            'theme://css/fonts/Crozet·te-Regular.woff':
                name: Crozet·te-Regular.woff
                type: font/woff
                size: 80296
                path: 'theme://css/fonts/Crozet·te-Regular.woff'
publish_date: '11-06-2024 15:37'
---

La·e Crozet·te est un·e caractère inclusif·ve et fleuri·e, inspiré·e de la broderie en point de croix. Avec la volonté de reprendre ces lettres brodées traditionnelles afin de leur donner un nouveau souffle pour des créations originales et engagées, lae Crozet·te a été pensé·e pour des titrages et textes courts principalement. On peut l’imaginer dans les mains marquées par le labeur d'un·e prince·sse queer qui compléterait l’ouvrage de ses aïeux.