document.addEventListener("DOMContentLoaded", function () {

    // Cloud

    const helloElement = document.querySelector('#hello');

    if (sessionStorage.getItem('cloudDisplay') === 'true') {
        helloElement.classList.add('animated');

        helloElement.addEventListener('animationend', () => {
            helloElement.classList.remove('animated');
        });
    }

    // Slideshow

    const fontFamilies = [];

    document.fonts.forEach(fontFace => {
        if (fontFace.weight === 'normal') {
            fontFamilies.push(fontFace.family);
        }
    });

    let availableFontFamilies = [...fontFamilies];

    let previousSelectedFamily = null;

    function getRandomFontFamily() {
        if (availableFontFamilies.length === 0) {
            availableFontFamilies = [...fontFamilies];
        }

        let randomIndex;
        let selectedFamily;

        do {
            randomIndex = Math.floor(Math.random() * availableFontFamilies.length);
            selectedFamily = availableFontFamilies[randomIndex];
        } while (selectedFamily === previousSelectedFamily);

        previousSelectedFamily = selectedFamily;
        availableFontFamilies.splice(randomIndex, 1);

        const items = document.querySelectorAll('#mots-content li');
        items.forEach(item => {
            item.style.fontFamily = selectedFamily;
        });
    }


    let items = document.querySelectorAll('#mots-content li');
    let currentIndex = 0;
    let speed = 600;

    function applyFontChange() {
        return new Promise((resolve) => {
            getRandomFontFamily();
            setTimeout(resolve, 40);
        });
    }

    async function typeWriter() {
        if (currentIndex < items.length) {
            const item = items[currentIndex];
            const text = item.textContent.trim();
            item.textContent = '';
            let i = 0;

            await applyFontChange();

            function type() {
                if (i < text.length) {
                    item.textContent += text.charAt(i);
                    i++;
                    setTimeout(type, speed);
                    item.classList.add("current-slide");
                } else {
                    item.classList.remove("current-slide");
                    currentIndex++;
                    typeWriter();
                }
            }

            type();
        } else {
            currentIndex = 0;
            typeWriter();
        }
    }

    typeWriter();
});

const array = [
    "I\u00B7El \u00E9tait chevalier\u00B7e, i\u00B7el \u00E9tait docteur\u00B7e en lettres, i\u00B7el \u00E9tait professeur\u00B7se, i\u00B7el \u00E9tait l\u2019auteur\u00B7ice d\u2019une vingtaine de volumes",
    "Je t\u2019aime comme un\u00B7e fou\u00B7lle, comme un\u00B7e soldat\u00B7e, comme un\u00B7e acteur\u00B7ice de cin\u00E9ma",
    "Jamais, depuis le commencement du monde, on ne vit mortel\u00B7le aussi ravissant\u00B7e",
    "Pendant un instant, j\u2019ai vu dans ce miroir le\u00B7a ho\u00B7femme que j\u2019allais \u00EAtre une fois adulte qui me regardait fixement. I\u00B7El avait l\u2019air inqui\u0117t\u00B7e et triste. Je me demandais si j\u2019\u00E9tais assez courageux\u00B7se pour grandir et devenir ell\u00B7ui",
    "De nouvelle\u00B7aux ho\u00B7femmes furent form\u00E9\u00B7es",
    "En discutant avec les cr\u00E9ateur\u00B7ices et les \u00E9tudiant\u00B7es, on se rend compte qu\u2019i\u00B7els n\u2019ont pas forc\u00E9ment envie de reproduire ces m\u00EAmes sch\u00E9mas. Certain\u00B7es disent m\u00EAme avoir honte de soutenir un syst\u00E8me dans lequel i\u00B7els ne croient plus et se demandent si y participer est de nature \u00E0 les rendre heureux\u00B7ses",
    "Je veux un\u00B7e gouin\u00B7e pour pr\u00E9sident\u00B7e. Je veux qu'i\u00B7el ait le sida, je veux que la\u00B7e premier\u00B7e ministre soit un\u00B7e tapet\u00B7te qui n'a pas la s\u00E9cu",
    "Je veux un\u00B7e pr\u00E9sidente qui vit sans clim, qui a fait la queue \u00E0 l\u2019h\u00F4pital, \u00E0 la CAF et au P\u00F4le Emploi, qui a \u00E9t\u00E9 ch\u00F4meur\u00B7se, licenci\u00E9\u00B7e \u00E9conomique, harcel\u00E9\u00B7e sexuellement, tabass\u00E9\u00B7e \u00E0 cause de son homosexualit\u00E9, et expuls\u00E9\u00B7e",
    "Et je veux savoir pourquoi ce que je demande n\u2019est pas possible; pourquoi on nous a fait gober qu\u2019un\u00B7e pr\u00E9sident\u00B7e est toujours un\u00B7e marionnet\u00B7te: toujours un\u00B7e micheton\u00B7ne et jamais un\u00B7e put\u00B7e. Toujours un\u00B7e patron\u00B7ne et jamais un\u00B7e travailleur\u00B7se. Toujours menteur\u00B7se, toujours voleur\u00B7se, et jamais puni\u00B7e",
    "I\u00B7Els disent qu\u2019i\u00B7els ont appris \u00E0 compter sur leurs propres forces. I\u00B7Els disent qu\u2019i\u00B7els savent ce qu\u2019ensemble i\u00B7els signifient. I\u00B7Els disent, que celle\u00B7ux qui revendiquent un langage nouveau apprennent d\u2019abord la violence",
    "I\u00B7Els disent, que celle\u00B7ux qui veulent transformer le monde s\u2019emparent avant tout des fusils. I\u00B7Els disent qu\u2019i\u00B7els partent de z\u00E9ro. I\u00B7Els disent que c\u2019est un\u00B7e mond\u00B7e nouvelle\u00B7au qui commence",
    "I\u00B7Els vous \u00E9crit du futur. Un futur plus lointain cette fois. Un futur o\u00F9 les chercheur\u00B7ses en langue fran\u00E7ais\u00B7e d\u00E9couvriront cette \u00E9trange p\u00E9riode de notre histoire o\u00F9 la\u00B7e binarit\u00E9 s\u2019\u00E9tait impos\u00E9\u00B7e jusque dans la\u00B7e langue"
]

document.addEventListener("DOMContentLoaded", function () {
    const length = array.length;
    const paragraphs = document.querySelectorAll(".sentence");
    let lastIndex = -1;

    paragraphs.forEach((paragraph) => {
        let randomIndex;
        do {
            randomIndex = Math.floor(Math.random() * length);
        } while (randomIndex === lastIndex);

        const sentence = array[randomIndex];
        paragraph.innerText = `${sentence}. ${sentence}`;
        lastIndex = randomIndex;
    });

    // Sidebar

    const arrow = document.querySelectorAll(".arrow");
    const sidebar = document.getElementById("hello");

    arrow[0].addEventListener('click', () => {
        if (sessionStorage.getItem('cloudDisplay') === 'true') {

            sidebar.classList.add("translate");
            arrow[0].classList.add("translate");
            sessionStorage.setItem('cloudDisplay', 'false');
            

        } else if (sessionStorage.getItem('cloudDisplay') === 'false' || sessionStorage.getItem('cloudDisplay') === null) {

            sidebar.classList.remove("translate");
            arrow[0].classList.remove("translate");
            sessionStorage.setItem('cloudDisplay', 'true');

        }
    });

});