<div id="sommaire-nav" markdown="1">

<div id="sommaire-content" markdown="1">

<div id="sommaire-content"  markdown="1">

* [Læ Queer Unicode Initiative](#quni)
* [Comment qunifier sa fonte ?](#qunifier)
	* [Tableau de læ QUNI](/quni/tableau/?target="_blank")
* [Tutoriels-vidéos de qunification](#tutos)
* [Proposer vos fontes qunifiées](#proposer)
* [En savoir plus sur læ QUNI](#savoir-plus)

</div>

</div>

</div>

<div class="content" markdown="1">

# Læ Queer Unicode Initiative (QUNI) <a class="anchor" id="quni"></a>

Læ QUNI est la recette qui permet aux fontes post-binaires d'être compatibles entre elles. I·el permet également l'activation de remplacements automatiques lors de la saisie au clavier.

En avril 2022, Bye Bye Binary a sorti une première version du QUNI qui a été corrigée et augmant pour la sortie de la nouvelle typothèque en septembre 2024.

<details markdown="1">

<summary markdown="1">

## Comment qunifier sa fonte ? <a class="anchor" id="qunifier"></a>

</summary >

Qunifier, c'est-à-dire encoder des caractères post-binaires dans une fonte selon læ QUNI, est easy. Il suffit de dessiner les glyphes dans votre éditeur de fonte préféré (Glyphs, FontLab, FontForge, Birdfont, etc.), de leur attribuer <!-- le nom de production --> le nom et le code Unicode adéquats en consultant le [tableau de læ QUNI](/quni/tableau/?target="_blank"), et enfin de rédiger les [features OpenType](https://genderfluid.space/lexiquni.html#f?target="_blank") qui activeront les remplacements automatiques à la saisie.

Si vous travaillez sur Glyphs, vous pouvez aussi simplement partir du [fichier Glyphs précuit](https://gitlab.com/bye-bye-binary/typotheque/-/raw/master/fichiers-vides/fichier-vide.glyphs?inline=false).

Pour l’écriture des [features OpenType](https://genderfluid.space/lexiquni.html#f?target="_blank"), vous pouvez puiser dans les [fichiers d’exemples](https://gitlab.com/bye-bye-binary/typotheque/-/tree/master/features-samples?target="_blank") commentés et/ou vous référer au [tableau de læ QUNI](/quni/tableau/?target="_blank").

Ces fichiers documentent les prérequis et l’écriture des features OpenType recommandées par læ QUNI. Pour rendre ces features fonctionnelles dans votre fonte, il est nécessaire que la fonte contienne les 6 caractères suivants&#8239;:

* un point médian ```periodcentered``` U+00B7
* un point médian ajusté à hauteur des capitales — pour cela ajouter un glyphe nommé ```periodcentered.case``` (tu peux par exemple copier coller ton médian standard et le remonter un poil&#8239;!)
* des points de suspension ```ellipsis``` U+2026
* un e macron bas-de-casse «&#8239;ē&#8239;» ```emacron``` U+0113
* un E macron capitale «&#8239;Ē&#8239;» ```Emacron``` U+0112

Pour permettre aux personnes qui n’ont pas accès au point médian sur leur clavier de taper des caractères post-binaires, on met en place le remplacement automatique d’une suite des deux points bas ```period period``` en point médian ```periodcentered```. Dans la volée, on remplace aussi les suites de trois points bas ```period period period``` en points de suspension ```ellipsis```, pour éviter les conflits&#8239;:
```
lookup point {
    sub period period by periodcentered;
    sub period period period by ellipsis;
    } point;
```

On crée une classe ```@Uppercase``` dans laquelle on met toutes capitales et on remplace le point médian normal par ```periodcentered.case``` quand il est entouré de capitales&#8239;:
```
	sub @Uppercase periodcentered' by periodcentered.case;
	sub periodcentered' @Uppercase by periodcentered.case;
```

Les ē et Ē macron nous seront utiles pour marquer un rappel de l’accent grave dans les mots avec terminaison en «&#8239;-er/-ère&#8239;» comme *boulanger·e → boulangēre* ou *ouvrier·e → ouvriēre*. On remplace le ```e``` ou ```egrave``` par un ```emacron``` dans les mots boulangēr·e, ouvriēr·e, sēc·he, poēte·sse&#8239;:
```
lookup macron {
    sub e' r periodcentered by emacron;
    sub egrave' r periodcentered by emacron;
    sub E' R periodcentered.case by Emacron;
    sub Egrave' R periodcentered.case by Emacron;
	sub e' c periodcentered h by emacron;
	sub egrave' c periodcentered h by emacron;
	sub E' C periodcentered.case H by Emacron;
	sub Egrave' C periodcentered.case H by Emacron;
	sub eacute' t e periodcentered s s by emacron;
	sub egrave' t e periodcentered s s by emacron;
	sub Eacute' T E periodcentered.case S S by Emacron;
	sub Egrave' T E periodcentered.case S S by Emacron;
	sub e' t periodcentered e by emacron;
	sub egrave' t periodcentered e by emacron;
	sub E' T periodcentered.case E by Emacron;
	sub Egrave' T periodcentered.case E by Emacron;
    } macron;
```

**Les features fonctionnent en cascade donc l’ordre dans lequel elles sont rédigées a de l’importance**. Par exemple, on remplace d’abord les points médians en cohabitation avec des capitales par des points médians de capitales ```periodcentered.case``` avant de lister des remplacements qui font appel à ce même ```periodcentered.case```.

</details>

##

<details markdown="1">

<summary markdown="1">

## Tutoriels-vidéos de qunification <a class="anchor" id="tutos"></a>

</summary>

#### FontForge
<iframe title="BBB-encodage-fontforge" src="https://videos.domainepublic.net/videos/embed/933d1e67-4a2c-4d53-8d7c-119e4aeb304f" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

<iframe title="BBB-features-base-fontforge" src="https://videos.domainepublic.net/videos/embed/3d0f570f-8da3-463b-95f6-6fee8fff4498" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

 
#### Glyphs
<iframe title="Tuto QUNI - La base dans Glyphs" src="https://videos.domainepublic.net/videos/embed/bbbbd16c-18e0-4ceb-97cf-bbe33a0a1308" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

<iframe title="Tuto QUNI - comment faire une fondue dans Glyphs" src="https://videos.domainepublic.net/videos/embed/0315b69d-40c2-4768-bade-2c4ff6648dc5" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

#### FontLab
<!-- vidéo à faire et ajouter -->
À venir

#### RoboFont
<!-- vidéo à faire et ajouter -->
À venir

#### BirdFont
<!-- vidéo à faire et ajouter -->
À venir

</details >

##

<details markdown="1">

<summary markdown="1">

## Proposer vos fontes qunifiées <a class="anchor" id="proposer"></a>

</summary>

### 43 ligatures post-binaires requises

Si votre fonte repose sur un principe de ligatures, un minimum de 43 d’entre elles sont nécessaires à son bon fonctionnement, en voici la liste&#8239;:

```
a_e
a_o
a_u_l_a
c_h
c_q
d_e
d_u_d_e_l_a
e_a
eacute_e
e_t
e_s
e_u
f_f_e
f_v
g_u
i_e
l_e
l_l
l_u
m_p
n_e
n_n
o_f
o_idieresis
o_t
p_m
p_e
r_e
r_i
r_s
s_c
s_e
s_l
s_s
s_t
t_e
t_t
u_e
u_l
x_c
x_l
x_l_l
x_s
```
Si votre fonte post-binaire fonctionne avec un autre principe que des ligatures, écrivez-nous à [typotheque@genderfluid.space](mailto:typotheque@genderfluid.space).

### Crash-test 

Passez votre fonte dans notre fichier crash-test au format [.rtf](2024_BBB_QUNI_test.rtf) ou [.indd](2024_BBB_QUNI_test.indd.zip).

### Organisez vos fichiers

Vos fichiers doivent être organisés selon une architecture précise avec ces dossiers&#8239;:
* **otf** (contenant vos exports en .otf)
* **ttf** (contenant vos exports en .ttf)
* **webfonts** (contenant vos exports en .woff et .woff2)
* **source** (contenant vos fichiers-sources en .ufo, .glyphs, .vfc, etc.)

Un certain nombre de fichiers doivent également être présents à la racine&#8239;:
* **README.md** (contenant le texte de présentation de la fonte)
* **Licence** (CUTE ou OIFL ou OFL ou CC, etc.)
* **Specimen.pdf** (un spécimen de la fonte au format pdf)

En guise d’exemples, vous pouvez regarder comment sont organisés les repository des fontes sur le [GitLab BBB](https://gitlab.com/bye-bye-binary?target="_blank").

### Donnez-nous quelques informations

Nous avons besoin de quelques informations qui figureront sur la page de votre fonte une fois publiée&#8239;:

* __Nom de la fonte*__&#8239;:
* __Auteur·ice(s)*__&#8239;:
* __Un lien vers votre site*__&#8239;:
* __Graisse(s)__&#8239;: (regular, italique, gras, condensé, etc.)
* __Version(s)__&#8239;: (normal, rounded, mono, etc.)

### Écrivez-nous

Une fois toutes les étapes précédentes complétées, écrivez-nous un mail avec un **.zip** de vos fichiers et toutes les informations en corps de mail à cette adresse&#8239;: [typotheque@genderfluid.space](mailto:typotheque@genderfluid.space)

</details>

##

<details markdown="1">

<summary markdown="1">

## En savoir plus sur læ QUNI <a class="anchor" id="savoir-plus"></a>

</summary>

### Le standard Unicode comme terrain vague
Cette grande prairie de jeux et d’expériences inclusives est aménagée à l’intérieur de la [Supplementary Private Use Area-A (PUA-A)](https://en.wikipedia.org/wiki/Private_Use_Areas#PUA-A?target="_blank") du très industriel [Standard Unicode](https://fr.wikipedia.org/wiki/Unicode?target="_blank"), centré sur l’alphabet latin. On n’a pas le choix, c’est le “leading standard” qui est utilisé par tous les téléphones, ordinateurs et logiciels qui nous entourent. Pour y injecter læ QUNI, on campe dans un bloc terrain vague en bordure, et c’est évidement la zone la plus cool. Nous nous sommes inspiré·es du [Medieval Unicode Font Initiative (MUFI)](https://mufi.info?target="_blank"), un projet visant à coordonner l’encodage et l’affichage de caractères médiévaux écrits en alphabet latin.

#### Pour les dessinateur·ices
Pour dessiner des caractères post-binaires dans des fontes, læ QUNI est construit·e autour de trois éléments-outils&#8239;:

* **Quoi**&#8239;: la liste la plus complète possible des caractères à dessiner.
* **Où** les mettre&#8239;: le tableau des points de codage Unicode qui y correspondent.
* **Comment** les faire apparaître (y accéder)&#8239;: les fonctionnalités OpenType avec lesquelles équiper les fontes. Tous ces outils sont repris dans le tableau de læ QUNI.

##### Quoi - la liste des caractères post-binaires
La liste des caractères post-binaires reprend un maximum de caractères post-binaires qui ont été injectés par les dessinateur·ices des premières fontes de la typothèque. Ce sont très largement des caractères post-binaires utilisés pour couvrir les suffixes genrés de la langue française. C’est une liste très ouverte qui comprend des formes de «&#8239;base&#8239;», des formes «&#8239;fondues&#8239;» (pour inclure le s du pluriel par exemple), des signes diacritiques et l’[Acadam](https://genderfluid.space/lexiquni.html?target="_blank").

##### Où mettre ces caractères post-binaires
Le [tableau de læ QUNI](/quni/tableau/?target="_blank") permet de savoir dans quelle case mettre quel caractère, c'est-à-dire quel code Unicode (U+...) leur attribuer. Il indique aussi les noms de production à donner aux caractères post-binaires (exemple: eacute_e), utilisés entre autres dans les [features OpenType](https://genderfluid.space/lexiquni.html#f?target="_blank"). Ces codes Unicode et noms de production permet d’être le plus largement compatible.

##### Comment accéder à ces caractères post-binaires
Les [features OpenType](https://genderfluid.space/lexiquni.html#f?target="_blank") permettent d’appeler les caractères post-binaires grâce à un système de remplacements automatiques. Ainsi, il suffit à l’utilisateur·ices de la fonte de rédiger en inclusif en utilisant le point médian (·) pour appeler le caractère adéquat. Par exemple é&#8201;·&#8201;e est automatiquement remplacé par le caractère é·e grâce au code de substitution ```sub eacute periodcentered e by eacute_e;```

#### Les fichiers

* [Le fichier Glyphs précuit](https://gitlab.com/bye-bye-binary/typotheque/-/tree/master/fichiers-vides)
* [Les fichiers exemples des fonctionnalités Opentype](https://gitlab.com/bye-bye-binary/typotheque/-/tree/master/features-samples?target="_blank")
* [Le tableau de læ QUNI](/quni/tableau/?target="_blank")

<!-- iframe du tableau en css grid -->

Pour les curieux·ses:
* [Le tableau du QUNI extensif dans sa PUA complète](https://gitlab.com/bye-bye-binary/typotheque/-/blob/master/quni-mapping/spua-a-bbb-plane.xls?target="_blank")


### Pour qui ? Pour plein de nous !

#### Læ QUNI est développé·e :

* Pour que les utilisateur·ices des fontes puissent accéder et utiliser les caractères post-binaires inclusif·ves dans leurs textes d’une manière compatible entre différentes fontes. Voir notre [Mode d’emploi](../mode-demploi/).
* Pour que les dessinateur·ices des fontes puissent poser les caractères post-binaires dans les cases Unicode communes. Voir [le tableau de læ QUNI](/tableau/)
* Pour que les mêmes dessinateur·ices des fontes puissent utiliser les [features OpenType pré-rédigées](https://gitlab.com/bye-bye-binary/typotheque/-/tree/master/features-samples?target="_blank") comme base.

### Quatre modes de fonctionnement
Nous avons répertorié plusieurs modes de fonctionnement des fontes inclusives existantes.

#### Ligatures
Il y a deux possibilités de ligatures au moins. Les ligatures de base (onglet «&#8239;base&#8239;» dans le tableau du QUNI) s’opèrent entre deux caractères séparés par un point médian.

![Exemple de quelques caractères post-binaires ligaturés dans la fonte Baskervvol](image-4.webp)

Les ligatures plus fondues (onglet «&#8239;fondue&#8239;» dans [le tableau de læ QUNI](/quni/tableau/?target="_blank")) permettent des agglomérations de plus de deux caractères, par exemple dans les formes au pluriel.

![Exemple de ligatures de plus en plus fondues dans la fonte Baskervvol](image-5.webp)

#### Formes alternatives non-binaires
Les caractères non-binaires sont conçus pour permettre un accord débinarisé ne reposant pas sur la fusion entre les terminaisons masculines et féminines. Par exemple, l’usage d’un «&#8239;x&#8239;» au dessin particulier.

![Exemple de quelques caractères post-binaires alternatifs ə dans la fonte Combine](image-6.webp)

#### Signes diacritiques
Les signes diacritiques souscrits ont des accents sous les lettres qui permettent de marquer les terminaisons genrées, comme des bornes indiquant le passage d’un genre à un autre.

![Exemple de quelques caractères post-binaires avec diacritiques dans la fonte Adelphe](image-7.webp)

### Acadam
Les fontes reposant sur l’[Acadam](https://genderfluid.space/lexiquni.html#a?target="_blank") permettent de traduire un texte écrit en inclusif avec le point médian en grammaire non-genrée.

![Exemple d’usage de l’Acadam dans la fonte Deliæ](image-8.webp)

### Læ QUNI gonflable
Læ QUNI est prêt·e à de très nombreux ajouts, c’est une initiative blobesque. De plus en plus de fontes se font garnir de caractères post-binaires. Chacun d’entre eux s’est vu attribuer une valeur Unicode de la [Supplementary Private Use Area-A (PUA-A)](https://en.wikipedia.org/wiki/Private_Use_Areas#PUA-A?target="_blank") et peut être enrichi de quinze variantes de dessin (alternates), et ça dans chaque fonte. L’initiative a des perspectives d’agrandissement, entre autres pour d’autres langues et systèmes d’écriture, puisqu’en tout ce sont 3496 cases qui sont disponibles pour y injecter de nouvelles propositions de caractères post-binaires, et 52440 variantes&#8239;!

#### Le tableau d’occupation de la PUA
![Le tableau de la PUA](image-9.webp)
[Le tableau de læ QUNI extensif dans sa PUA complète](https://gitlab.com/bye-bye-binary/typotheque/-/blob/master/quni-mapping/spua-a-bbb-plane.xls?target="_blank")

#### Le système proposé pour les caractères alternatifs
![Le système proposé pour les alternatives](image-10.webp)
Attention&#8239;: le standard Unicode fonctionne en hexadécimal, pour respecter ce système de mesure il y a 15 variantes de dessins possibles derrière un glyphe dit «&#8239;parent&#8239;» (15+1=16). De cette manière, chaque Unicode d’un glyphe parent se termine par «&#8239;0&#8239;», la première variante par «&#8239;1&#8239;», jusqu’à la quinzième variante qui se termine par «&#8239;F&#8239;». Nous allons donc de «&#8239;0&#8239;» à «&#8239;F&#8239;» comme ceci : U+F1000, U+F1001, U+F1002, U+F1003, U+F1004, U+F1005, U+F1006, U+F1007, U+F1008, U+F1009, U+F100A, U+F100B, U+F100C, U+F100D, U+F100E, U+F100F.

</details>

</div>