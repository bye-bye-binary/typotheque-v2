---
title: Ouvrières
fontsfiles:
    -
        style: Affamées
        font:
            'theme://css/fonts/Ouvrieres-affamees.woff':
                name: Ouvrieres-affamees.woff
                type: font/woff
                size: 214244
                path: 'theme://css/fonts/Ouvrieres-affamees.woff'
    -
        style: Rassasiées
        font:
            'theme://css/fonts/Ouvrieres-rassasiees.woff':
                name: Ouvrieres-rassasiees.woff
                type: font/woff
                size: 210532
                path: 'theme://css/fonts/Ouvrieres-rassasiees.woff'
    -
        style: Agricultrices
        font:
            'theme://css/fonts/Ouvrieres-agricultrices.woff':
                name: Ouvrieres-agricultrices.woff
                type: font/woff
                size: 213120
                path: 'theme://css/fonts/Ouvrieres-agricultrices.woff'
    -
        style: Exploratrices
        font:
            'theme://css/fonts/Ouvrieres-exploratrices.woff':
                name: Ouvrieres-exploratrices.woff
                type: font/woff
                size: 216476
                path: 'theme://css/fonts/Ouvrieres-exploratrices.woff'
    -
        style: Soldates
        font:
            'theme://css/fonts/Ouvrieres-soldates.woff':
                name: Ouvrieres-soldates.woff
                type: font/woff
                size: 205176
                path: 'theme://css/fonts/Ouvrieres-soldates.woff'
sentence: 'Aimé·e et un peu perdu·e, la·e fourmi·e navigue jusqu''à la miet·te.'
font:
    author: 'Laure Azizi'
    website: 'https://laureazizi.fr/'
    date: '2024'
    license: CUTE
    download: 'https://gitlab.com/bye-bye-binary/ouvrieres/-/archive/main/ouvrieres-main.zip'
    link: 'https://gitlab.com/bye-bye-binary/ouvrieres#'
    fontspecimen: 'https://laureazizi.fr/imports/specimen_alphabet.jpg'
publish_date: '11-09-2024 16:49'
---

Les fourmis fourmillent dans la forêt à la recherche de miettes succulentes. Tantôt affamées, tantôt combattantes, elles adaptent leurs formations selon deux axes variables pour créer six graisses, afin de s'adapter à toutes les situations. Elles nous guident dans les galeries sinueuses, peut-être jusqu'à la reine ? Conçu lors d'une initiation à la typographie variable par Eugénie Bidaut à l'école d’art de La Cambre à Bruxelles, ce caractère organique est assemblé et construit par des milliers de fourmis ouvrières. Leur travail acharné donne vie aux glyphes, ligatures et ligatures incluses animées qui composent cette grande fourmilière.