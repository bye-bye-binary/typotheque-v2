---
title: PubliFluor
font:
    author: 'Groupe de recherche Crickx'
    website: 'http://publifluor.osp.kitchen/'
    license: CUTE
    download: 'https://gitlab.constantvzw.org/osp/foundry.crickx/-/archive/master/foundry.crickx-master.zip'
    link: 'https://gitlab.constantvzw.org/osp/foundry.crickx/'
    fontspecimen: 'http://publifluor.osp.kitchen/specimen.html'
    date: '2024'
fontsfiles:
    -
        style: Normale
        font:
            'theme://css/fonts/PubliFluorNormale.woff':
                name: PubliFluorNormale.woff
                type: font/woff
                size: 39456
                path: 'theme://css/fonts/PubliFluorNormale.woff'
sentence: 'Tu as vu l’allure qu’i·els ont mes rond·es ?'
publish_date: '11-09-2024 15:45'
published: false
---

Autodidacte, Chrystel Crickx découpait à la main et vendait à la pièce des lettres dans son magasin Publi Fluor à Schaerbeek. Découpées dans du vinyle adhésif entre 1975 et 2000 pour des usages signalétiques et publicitaires locaux, ces lettres ont depuis été numérisées et rendues plus largement accessibles à des utilisateur·ices en dehors des frontières belges et dans d'autres contextes.