---
title: 'BBB Poppins TN'
fontsfiles:
    -
        style: 'Display Regular'
        font:
            'theme://css/fonts/BBBPoppinsTN-DisplayRegular.woff':
                name: BBBPoppinsTN-DisplayRegular.woff
                type: font/woff
                size: 34312
                path: 'theme://css/fonts/BBBPoppinsTN-DisplayRegular.woff'
    -
        style: 'Display Italic'
        font:
            'theme://css/fonts/BBBPoppinsTN-DisplayRegularItalic.woff':
                name: BBBPoppinsTN-DisplayRegularItalic.woff
                type: font/woff
                size: 36744
                path: 'theme://css/fonts/BBBPoppinsTN-DisplayRegularItalic.woff'
    -
        style: 'Display SemiBold'
        font:
            'theme://css/fonts/BBBPoppinsTN-DisplaySemiBold.woff':
                name: BBBPoppinsTN-DisplaySemiBold.woff
                type: font/woff
                size: 35260
                path: 'theme://css/fonts/BBBPoppinsTN-DisplaySemiBold.woff'
    -
        style: 'Display SemiBold Italic'
        font:
            'theme://css/fonts/BBBPoppinsTN-DisplaySemiBoldItalic.woff':
                name: BBBPoppinsTN-DisplaySemiBoldItalic.woff
                type: font/woff
                size: 36928
                path: 'theme://css/fonts/BBBPoppinsTN-DisplaySemiBoldItalic.woff'
    -
        style: 'Display Bold'
        font:
            'theme://css/fonts/BBBPoppinsTN-DisplayBold.woff':
                name: BBBPoppinsTN-DisplayBold.woff
                type: font/woff
                size: 35756
                path: 'theme://css/fonts/BBBPoppinsTN-DisplayBold.woff'
    -
        style: 'Display Bold Italic'
        font:
            'theme://css/fonts/BBBPoppinsTN-DisplayBoldItalic.woff':
                name: BBBPoppinsTN-DisplayBoldItalic.woff
                type: font/woff
                size: 37404
                path: 'theme://css/fonts/BBBPoppinsTN-DisplayBoldItalic.woff'
    -
        style: 'Display Black'
        font:
            'theme://css/fonts/BBBPoppinsTN-DisplayBlack.woff':
                name: BBBPoppinsTN-DisplayBlack.woff
                type: font/woff
                size: 35876
                path: 'theme://css/fonts/BBBPoppinsTN-DisplayBlack.woff'
    -
        style: 'Display Black Italic'
        font:
            'theme://css/fonts/BBBPoppinsTN-DisplayBlackItalic.woff':
                name: BBBPoppinsTN-DisplayBlackItalic.woff
                type: font/woff
                size: 36700
                path: 'theme://css/fonts/BBBPoppinsTN-DisplayBlackItalic.woff'
    -
        style: 'Text Black'
        font:
            'theme://css/fonts/BBBPoppinsTN-TextBlack.woff':
                name: BBBPoppinsTN-TextBlack.woff
                type: font/woff
                size: 35856
                path: 'theme://css/fonts/BBBPoppinsTN-TextBlack.woff'
    -
        style: 'Text Black Italic'
        font:
            'theme://css/fonts/BBBPoppinsTN-TextBlackItalic.woff':
                name: BBBPoppinsTN-TextBlackItalic.woff
                type: font/woff
                size: 36804
                path: 'theme://css/fonts/BBBPoppinsTN-TextBlackItalic.woff'
    -
        style: 'Text Bold'
        font:
            'theme://css/fonts/BBBPoppinsTN-TextBold.woff':
                name: BBBPoppinsTN-TextBold.woff
                type: font/woff
                size: 35728
                path: 'theme://css/fonts/BBBPoppinsTN-TextBold.woff'
    -
        style: 'Text Bold Italic'
        font:
            'theme://css/fonts/BBBPoppinsTN-TextBoldItalic.woff':
                name: BBBPoppinsTN-TextBoldItalic.woff
                type: font/woff
                size: 37288
                path: 'theme://css/fonts/BBBPoppinsTN-TextBoldItalic.woff'
    -
        style: 'Text Regular'
        font:
            'theme://css/fonts/BBBPoppinsTN-TextRegular.woff':
                name: BBBPoppinsTN-TextRegular.woff
                type: font/woff
                size: 34180
                path: 'theme://css/fonts/BBBPoppinsTN-TextRegular.woff'
    -
        style: 'Text Regular Italic'
        font:
            'theme://css/fonts/BBBPoppinsTN-TextRegular.woff':
                name: BBBPoppinsTN-TextRegular.woff
                type: font/woff
                size: 34180
                path: 'theme://css/fonts/BBBPoppinsTN-TextRegular.woff'
    -
        style: 'Text SemiBold'
        font:
            'theme://css/fonts/BBBPoppinsTN-TextSemiBold.woff':
                name: BBBPoppinsTN-TextSemiBold.woff
                type: font/woff
                size: 35204
                path: 'theme://css/fonts/BBBPoppinsTN-TextSemiBold.woff'
    -
        style: 'Text SemiBold Italic'
        font:
            'theme://css/fonts/BBBPoppinsTN-TextSemiBoldItalic.woff':
                name: BBBPoppinsTN-TextSemiBoldItalic.woff
                type: font/woff
                size: 36888
                path: 'theme://css/fonts/BBBPoppinsTN-TextSemiBoldItalic.woff'
            'theme://css/fonts/BBBPoppinsTN-TextRegular.woff':
                name: BBBPoppinsTN-TextRegular.woff
                type: font/woff
                size: 34180
                path: 'theme://css/fonts/BBBPoppinsTN-TextRegular.woff'
sentence: 'Tous·tes spectateur·ices, ami·es, professionnel·les, invité·es, comédien·nes, acteur·ices, inclusif·ves.'
font:
    author: 'Eugénie Bidaut & Camille°Circlude et al.'
    website: 'https://genderfluid.space/'
    date: '2023'
    license: OFL
    download: 'https://gitlab.com/bye-bye-binary/bbb-poppins-tn/-/archive/main/bbb-poppins-tn-main.zip'
    link: 'https://gitlab.com/bye-bye-binary/bbb-poppins-tn/-/tree/main'
    fontspecimen: 'https://gitlab.com/bye-bye-binary/bbb-poppins-tn/-/raw/main/BBBPopinsTN_specimen-glyphes-inclusifs.pdf?inline=false'
publish_date: '10-06-2024 16:40'
---

La BBB Poppins TN est un fork post-binaire de la fonte libre Poppins dessinée par Jonny Pinhorn. La fonte post-binaire, initialement dessinée par Eugénie Bidaut et Camille°Circlude pour l’usage du Théâtre National Wallonie-Bruxelles, est maintenant distribuée sous licence OFL pour l’usage de tous·tes.

La Poppins originale a été dessinée par Indian Type Foundry, Jonny Pinhorn, Ninad Kale.