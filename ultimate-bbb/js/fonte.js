function sliderChange() {
    const slider_value = document.getElementById("font_size_slider"); 
    let font_size = slider_value.value;
    let text = document.getElementById("font-sentence");
    let label = document.getElementById("slider_label");
    text.style.fontSize = font_size + "pt";
    label.innerHTML = font_size.toString() + " pt"; 
}

let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function autoSlide() {
  plusSlides(1);
  setTimeout(autoSlide, 3000);
}

setTimeout(autoSlide, 3000);

function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("glyphs");
  let dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}

document.addEventListener("DOMContentLoaded", () => {
  setTimeout(function () {
      document.querySelector('#font-sentence p').style.visibility = 'visible'
  }, 50)
});
